<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::domain('www.pasarpedia.id')->group(function(){
	Route::get('/','UtamaController@index')->name('utama.index');
	Route::post('/','UtamaController@store')->name('utama.store');
});

Route::get('login', 'AuthController@getLogin')->name('login');
Route::post('login', 'AuthController@postLogin')->name('login');
Route::get('register', 'AuthController@getRegister')->name('register');
Route::post('register', 'AuthController@postRegister')->name('register');
Route::get('logout', 'AuthController@getLogout')->name('logout')->middleware('auth');

// Auth::routes();

// Route::domain('apps.pasarpedia.id')->namespace('Apk')->group(function () {
Route::prefix('apps')->namespace('Apk')->group(function (){
	Route::get('/','IndexController@index')->name('apk.index');
	Route::get('cari','IndexController@cari')->name('apk.cari');
	Route::get('detail/{id}', 'IndexController@getDetail')->name('apk.detail');
	Route::get('member', 'IndexController@getMember')->name('apk.member')->middleware('auth');
	Route::get('member/profil', 'IndexController@getProfil')->name('apk.profil')->middleware('auth');
	Route::post('member/profil/update', 'IndexController@postProfil')->name('apk.profilUpdate')->middleware('auth');
	Route::post('member/profil/password', 'IndexController@postPassword')->name('apk.password')->middleware('auth');
	Route::get('member/keranjang', 'IndexController@getKeranjang')->name('apk.keranjang')->middleware('auth');
	Route::get('member/order', 'IndexController@getOrder')->name('apk.order')->middleware('auth');
	Route::get('member/order/{id}', 'IndexController@getOrderDetail')->name('apk.order-detail')->middleware('auth');
});

// Route::domain('{uid}.pasarpedia.id')->middleware('checkToko')->group(function () {
Route::prefix('u/{uid}')->middleware('checkToko')->group(function () {
    Route::get('/','IndexController@index')->name('index');
    Route::get('cari','IndexController@cari')->name('index.cari');
	Route::get('produk/{id}/{slug}', 'ProdukController@show')->name('produk.show');
	Route::resource('produk', 'ProdukController',['except' => [
	    'show'
	]]);
	Route::get('kategori/{id}/{slug}', 'KategoriController@show')->name('kategori.show');
	Route::get('promo', 'KategoriController@promo')->name('kategori.promo');
	Route::get('diskon', 'KategoriController@diskon')->name('kategori.diskon');
	Route::get('terlaris', 'KategoriController@terlaris')->name('kategori.terlaris');
	Route::get('terbaru', 'KategoriController@terbaru')->name('kategori.terbaru');

	Route::middleware('auth')->group(function(){
		Route::resource('order', 'OrderController');
		Route::post('keranjang/ubah/{id}', 'KeranjangController@ubah')->name('keranjang.ubah');
		Route::get('keranjang/antar', 'KeranjangController@antar')->name('keranjang.antar');
		Route::post('keranjang/bayar', 'KeranjangController@bayar')->name('keranjang.bayar');
		Route::post('keranjang/note', 'KeranjangController@postNote')->name('keranjang.note');
		Route::resource('keranjang', 'KeranjangController');
		Route::resource('newsToko', 'NewsController');
		Route::resource('member', 'MemberController',['except' => [
		    'show'
		]]);
		Route::get('member/profil', 'MemberController@profil')->name('member.profil');
		Route::get('member/toko', 'MemberController@toko')->name('member.toko');
		Route::post('member/profilUpdate', 'MemberController@profilUpdate')->name('member.profilUpdate');
		Route::post('member/password', 'MemberController@password')->name('member.password');
	});

	Route::get('orderanproses', 'IndexController@orderProses')->name('orderproses');
	Route::post('orderanproses', 'IndexController@postOrderProses');

});

// Route::prefix('admin')->namespace('Admin')->middleware('auth')->group(function () {
Route::prefix('admin')->namespace('Admin')->group(function (){
	Route::get('login', 'AuthController@index')->name('admin.index');
	Route::post('login', 'AuthController@auth')->name('admin.index');
	Route::post('logout', 'AuthController@logout')->name('admin.logout');
	Route::middleware('admin')->group(function(){
		Route::get('/','IndexController@index');
		Route::resource('beranda','IndexController');
		Route::resource('orderToko','OrderController');
		Route::get('toko/login/{id}', 'AuthController@toko')->name('toko.login');
		Route::get('toko/perpanjang','TokoController@perpanjang')->name('toko.perpanjang');
		Route::put('toko/perpanjang/aktifkan/{id}','TokoController@aktifkan')->name('toko.aktifkan');
		Route::put('toko/perpanjang/batalkan/{id}','TokoController@batalkan')->name('toko.batalkan');
		Route::get('toko/featured/{id}', 'TokoController@getFeatured')->name('toko.featured');
	    Route::resource('toko','TokoController');
	    Route::get('newsAdmin/tampil/{id}/{tampil}','NewsController@tampil')->name('newsAdmin.tampil');
	    Route::resource('newsAdmin','NewsController');
	    Route::resource('banks','BankController');
	    Route::resource('satuan','SatuanController');
	    Route::resource('slides','SlideController');
	    Route::resource('promos','PromoController');
	    Route::resource('information','InformationController');

	});
});

Route::prefix('tokoku')->namespace('AdminToko')->group(function (){
	Route::get('login', 'AuthController@index')->name('tokoku.index');
	Route::post('login', 'AuthController@auth')->name('tokoku.index');
	Route::get('reset-confirm', 'AuthController@reset2')->name('tokoku.reset2');
	Route::get('reset', 'AuthController@reset')->name('tokoku.reset');
	Route::post('reset', 'AuthController@resetact')->name('tokoku.reset');
	Route::post('lupa-password', 'AuthController@lupas')->name('tokoku.lupas');
	Route::post('logout', 'AuthController@logout')->name('tokoku.logout');

	Route::middleware('toko')->group(function(){
		Route::get('/','IndexController@index');
		Route::resource('dashboard','IndexController');
	    Route::resource('slide','SlideController');
	    Route::resource('cari','CariController');
	    Route::resource('kategori','KategoriController');
	    Route::resource('produk','ProdukController',['except' => [
		    'show'
		]]);
		Route::get('produk/gambar/{id}', 'ProdukController@gambar')->name('produk.gambar');
		Route::post('produk/gambarTambah/{id}', 'ProdukController@gambarTambah')->name('produk.storeGambar');
		Route::delete('produk/gambarHapus/{id}', 'ProdukController@gambarHapus')->name('produk.destroyGambar');
		
		Route::get('produk/stok/{id}', 'ProdukController@stok')->name('produk.stok');
		Route::post('produk/stokTambah/{id}', 'ProdukController@stokTambah')->name('produk.storeStok');
	    Route::resource('profil','ProfilController');
	    Route::resource('orderan','OrderanController');
	    Route::get('orderan/cetak/{id}','OrderanController@cetak')->name('orderan.cetak');
	    Route::get('orderan/cetak_besar/{id}','OrderanController@cetakbesar')->name('orderan.cetakbesar');
		Route::resource('memberku','MemberkuController');
		Route::get('memberku/login/{nohp}','AuthController@member')->name('memberku.login');
		Route::get('orderan/status/{status}', 'OrderanController@status')->name('orderan.status');
		Route::get('orderan/delete/{id}','OrderanController@getDelete')->name('orderan.delete');
	    Route::resource('laporan', 'LaporanController',['except' => [
		    'show'
		]]);
	    Route::get('laporan/perhari', 'LaporanController@perhari')->name('laporan.perhari');
	    Route::get('laporan/perbulan', 'LaporanController@perbulan')->name('laporan.perbulan');
	    Route::get('laporan/pertahun', 'LaporanController@pertahun')->name('laporan.pertahun');
	    Route::get('laporan/member', 'LaporanController@member')->name('laporan.member');
	    Route::get('news/tampil/{id}/{tampil}','NewsController@tampil')->name('news.tampil');
	    Route::resource('news','NewsController');
	    Route::resource('bank','BankController');
	    Route::resource('broadcast','BroadcastController');
	    Route::resource('referral','ReferralController');
	    Route::resource('metode','MethodController');
	    Route::resource('popup','PopupController');
	});
});

Route::prefix('referral')->namespace('Referral')->group(function (){
	Route::get('login','AuthController@login')->name('ref.login');
	Route::post('login','AuthController@loginact')->name('ref.login');
	Route::middleware('ref')->group(function(){
		Route::get('/','IndexController@index')->name('ref.index');
		Route::post('logout','AuthController@logout')->name('ref.logout');		
		Route::get('dashboard','IndexController@index')->name('ref.index');
		Route::get('member','MemberController@index')->name('ref.member');
		Route::get('transaksi','TransaksiController@index')->name('ref.transaksi');
		Route::get('transaksi/{date}','TransaksiController@perhari')->name('ref.transaksihari');
		Route::get('profil','ProfilController@index')->name('ref.profil');
		Route::put('profil/bank','ProfilController@bank')->name('ref.profilUpdateBank');
		Route::put('profil/pass','ProfilController@pass')->name('ref.profilUpdatePass');
	});
});

Route::get('/','UtamaController@index')->name('utama.index');
Route::post('/','UtamaController@store')->name('utama.store');

Route::get('clear_cache', function () {
    \Artisan::call('cache:clear');
    dd("Cache is cleared");
});

Route::get('test-email', function(){
	$order = \DB::table('otw_order')->first();
	\Mail::to('hendra.randy.nomura92@gmail.com')->send(new \App\Mail\KirimEmail($order));
});