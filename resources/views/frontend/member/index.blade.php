@extends('frontend.base')

@section('main')

<ul class="list-group">
  <li class="list-group-item bg-white mb-3 rounded-0">
  	<h5 class="card-title">{{ Auth::user()->nama }}</h5>
  	<span>{{ Auth::user()->nohp }}</span>
  </li>
  
  <li class="list-group-item bg-white mb-3 rounded-0"><a href="{{ route('member.profil') }}">Profil Anda</a></li>
  <li class="list-group-item bg-white rounded-0"><a href="{{ route('member.toko') }}">Profile Toko</a></li>
  <li class="list-group-item bg-white rounded-0"><a href="#">Panduan</a></li>
  <li class="list-group-item bg-white rounded-0"><a href="#">Syarat dan Ketentuan</a></li>
  
  <li class="list-group-item bg-white rounded-0">
	<a  href="{{ route('logout') }}">Logout</a>
  </li>
</ul>

@endsection