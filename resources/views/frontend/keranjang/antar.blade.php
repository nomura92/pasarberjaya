@extends('frontend.base')

@section('main')

<div class="container pt-3 pb-3">
    <div class="chatbox">
        <div class="bodi">
        <span class="tip tip-left"></span>
            <span><img src="https://kliker.id/img/smile.png"> Ini detail tagihan nya kak.. </span>
            <ul class="list-group bg-white mb-2 mt-2">
		        <li class="list-group-item rounded-0">
			        <b>Total Belanja</b>
			        <span class="float-right">{{ $duitKeranjang }}</span>
		        </li>
		        <li class="list-group-item rounded-0">
			        <b>Jasa belanja & Antar</b>
			        <span class="float-right">{{ rupiah($toko->ongkir) }}</span>
		        </li>
		        <li class="list-group-item rounded-0">
			        <b>Total Bayar
			        <span class="float-right">{{ rupiah($total+$toko->ongkir) }}</span></b>
		        </li>
	        </ul>
        </div>
    </div>
</div>

<form method="post" action="{{ route('keranjang.bayar') }}">
	@csrf
<div class="container pt-3 pb-3">
    <div class="chatbox">
        <div class="bodi">
        <span class="tip tip-left"></span>
        <span><img src="https://kliker.id/img/smile.png"> Barang akan kami siapkan, tulis Alamat Pengirimannya ya kak.. <i class="far fa-hand-point-down"></i></span>
    <ul class="list-group mb-2 mt-2">
		<li class="list-group-item rounded-0">
			<label>Alamat Pengiriman</label>
			<textarea class="form-control" id="alamat" name="alamat" required>{{ Auth::user()->alamat }}</textarea>
		</li>
		<li class="list-group-item rounded-0">
			<label>Nama Penerima</label>
			<input class="form-control" type="text" name="nama" id="nama" value="{{ Auth::user()->nama }}" required>
		</li>
		<li class="list-group-item rounded-0">
			<label>Nomor Handphone</label>
			<input class="form-control" type="text" name="nohp" id="nohp" value="{{ Auth::user()->nohp }}" required>
		</li>
	</ul>
        </div>
    </div>
</div>

<div class="container pt-3 pb-3">
    <div class="chatbox">
        <div class="bodi">
        <span class="tip tip-left"></span>
            <span><img src="https://kliker.id/img/smile.png"> Kakak mau bayar COD ditempat, atau transfer Bank ?</span>
	<ul class="list-group mb-2 mt-2">
		<li class="list-group-item rounded-0">
			<h6 class="mb-4"><b>COD (bayar ditempat)</b></h6>
			@foreach($metode->where('type','cod') as $k => $v)	
			<div class="form-group">
				<label for="cod{{$k}}">{{ $v->name }}</label>
				<input class="float-right" type="radio" name="metode" value="{{ $v->type.'-'.$v->id }}" id="cod{{$k}}" required>
			</div>
			@endforeach
		</li>
		<li class="list-group-item rounded-0">
			<h6 class="mb-4"><b>Bank Transfer</b><p>Transfer pada salah satu bank dibawah ini :</p></h6>
			@foreach($metode->where('type','tf') as $k => $v)	
			<div class="form-group">
				<label for="tf{{$k}}">{{ $v->name }}</label>
				<input class="float-right" type="radio" name="metode" value="{{ $v->type.'-'.$v->id }}" id="tf{{$k}}">
			</div>
			@endforeach
		</li>
	</ul>
	    </div>
    </div>
</div>
	
	<div class="container pt-3 pb-4">
    <div class="chatbox">
        <div class="bodi">
        <span class="tip tip-left"></span>
            <span><img src="https://kliker.id/img/smile.png"> Jika sudah benar semua, Klik tombol dibawah kak.. <i class="far fa-hand-point-down"></i> , secepatnya barang akan kami antar </span>
        </div>
    </div>
    </div>

	<div class="text-center pb-5">
		<button class="btn w-50 tombol rounded-0" type="submit">Order</button>
	</div>
</form>

<ul class="list-group bg-white mb-3 mt-1">
	@foreach($isiKeranjang as $item)
	<li class="list-group-item rounded-0 clearfix">
		<div class="media">
			@if($item->gambar!='')
			<img class="media-left" width="80" src="{{ asset('img/produk/'.$item->gambar) }}">
			@else
			<img class="media-left" width="80" src="https://via.placeholder.com/350x150.png?text=IMAGE NOT FOUND">
			@endif
			<div class="media-body pl-3">
				<h6 class="card-title">{{$item->nm_produk}}</h6>
				<p class="card-text">
					{{ rupiah($item->harga-$item->diskon) }} / <small>{{ $item->satuan }}</small> <br>
					Jumlah {{ $item->jumlah }}<br>
					{{ rupiah(($item->harga-$item->diskon)*$item->jumlah) }}
				</p>
			</div>
			<div class="media-right" style="width:30px;">
				<form action="{{ route('keranjang.destroy', $item->id_detail)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn" type="submit"><i class="fa fa-trash"></i></button>
                </form>
			</div>
		</div>
	</li>
	@endforeach
</ul>

@endsection