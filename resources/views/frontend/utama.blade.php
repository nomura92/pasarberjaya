<style>
.cart {
  background-color: #ff8e23;
  border: none;
  border-radius: 5px;
  color: white;
  padding: 12px 16px;
  font-size: 16px;
  cursor: pointer;
  margin-top: 20px;
}

/* Darker background on mouse-over */
.btn:hover {
  background-color: #4e81ee;
}
</style>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-144209950-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-144209950-1');
</script>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Pasar segar, belanja mudah dari rumah - PASARPEDIA</title>
	<link rel="shortcut icon" href="images/pasarpediaicon.png">
	<meta property="og:image" content="http://www.pasarpedia.id/images/pasarpediaicon.png" />
	<meta name="keywords" content="Pasar tradisional, Pasar online, Aplikasi pasar">
    
	<!-- Bootstrap CSS -->
	<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
     <!-- CSS Custom -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
    
    <!-- favicon Icon -->
    <!--<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">-->
    <!-- CSS Plugins -->
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/animate.min.css') }}">
    <!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,300,700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
     <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

</head>
<body>

<div class="navbar-fixed-bottom" style="margin-bottom:10px; margin-left:10px">
    <a href="https://api.whatsapp.com/send?phone=6282384128800&amp;text=Halo,%20Saya%20mau%20order....."> <i class="fa fa-whatsapp" style="font-size:50px; color: #f6b20e"></i> </a>
</div>

<div id="home" class="primary-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				
				<div class="col-md-6">
					<div class="present">
					    <img style="margin-top:30px" src="images/logoweb.png">
						<h1> <font color="#e6153c">Pasar Segar, belanja mudah dari rumah</font> </h1>
						
						<h5> <font color="#e6153c"> Belanja ke pasar tradisional gak perlu repot lagi, yuk mulai belanja.. </font> </h5>
						
						<div>
                        <a href="http://apps.pasarpedia.id" target="_blank"> <button class="cart" style="margin-bottom:20px"><i class="fa fa-cart-plus"></i> Belanja via Website</button> </a>
                        <a href="https://play.google.com/store/apps/details?id=id.kliker.pasarpedia" target="_blank"><img style="width:200px" src="images/playstore.png" alt="Playstore"></a>
                        </div>
                	</div>
				</div>
				
				<div class="col-md-6">
					<div class="present_img">
						<img src="images/bannerweb.png" alt="image">
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>

</body>
<!-- JS Plugins -->
  </html>