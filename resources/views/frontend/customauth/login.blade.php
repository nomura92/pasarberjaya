@extends('frontend.base')

@section('main')

<div class="container">
		<div class="card-body">
			<form method="POST" action="{{ route('login') }}">
				@csrf
				<input type="hidden" name="id_toko" value="{{ $toko->id_toko }}">
				<div class="form-group">
					<label for="nohp">Nomor Whatsapp</label>
					<input class="form-control" type="number" name="nohp" id="nohp" value="{{ old('nohp') }}" required autofocus>
					@if ($errors->has('nohp'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('nohp') }}</strong>
                        </span>
                    @endif
				</div>
				<div class="form-group">
					<label for="password">Password</label>
					<input class="form-control" type="password" name="password" id="password" required>
					@if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
				</div>
				<div class="form-group">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" checked>
                        <label class="form-check-label" for="remember">Ingat saya</label>
                    </div>
                </div>
                <button class="btn btn-primary" type="submit">Login</button>
			</form>
		</div>
</div>

<div class="container pt-5 pb-3">
    <div class="chatbox">
        <div class="bodi">
        <span class="tip tip-left"></span>
            <span><img src="{{ asset('img/smile.png')}}"> kalau belum punya akun daftar dulu kak..</span>
            <p class="text-center"> <a href="{{ route('register') }}"> <b>DAFTAR</b> </a> </p>
        </div>
    </div>
</div>

<div class="container pt-5 pb-3">
    <div class="chatbox">
        <div class="bodi">
        <span class="tip tip-left"></span>
            <span><img src="{{ asset('img/smile.png')}}"> lupa password ?.. chat ke admin aja kak</span>
            <a href="https://api.whatsapp.com/send?phone={{ $toko->call_center }}&amp;text=Halo.." target="_blank"> <h6 style="color:#f6b20e; font-size:18px" class="text-center pt-3"><img src="http://pasarpedia.id/img/waicon.png"> Chat Whatsapp </h6>
  		    </a>
        </div>
    </div>
</div>

@endsection