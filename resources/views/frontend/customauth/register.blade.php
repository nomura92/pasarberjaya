@extends('frontend.base')

@section('main')

<div class="container pt-3 pb-3">
    <div class="chatbox">
        <div class="bodi">
        <span class="tip tip-left"></span>
            <span><img src="{{ asset('img/smile.png') }}"> Daftar dulu ya kak.. <i class="far fa-hand-point-down"></i> </span>
        </div>
    </div>
</div>

<div class="container">
    <div class="">
        <div class="card-body">
            <form method="post" action="{{ route('register') }}">
                @csrf
                <input type="hidden" name="uid" value="{{ Request()->uid }}">
                <div class="form-group row">
                    <label for="nohp" class="col-md-4 col-form-label text-md-right">Nomor Whatsapp *</label>
                    <div class="col-md-6">
                        <input id="nohp" type="number" class="form-control{{ $errors->has('nohp') ? ' is-invalid' : '' }}" name="nohp" value="{{ old('nohp') }}" required autofocus>

                        @if ($errors->has('nohp'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('nohp') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Buat password *') }}</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                
                <div class="form-group row">
                    <label for="ref" class="col-md-4 col-form-label text-md-right">KODE PROMO (jika ada)</label>
                    <div class="col-md-6">
                        <input id="ref" type="text" class="form-control{{ $errors->has('ref') ? ' is-invalid' : '' }}" name="ref" value="{{ old('ref') }}" autofocus>

                        @if ($errors->has('ref'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('ref') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Daftar') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="container pt-5 pb-3">
    <div class="chatbox">
        <div class="bodi">
        <span class="tip tip-left"></span>
            <span><img src="{{ asset('img/smile.png') }}"> Kalo sudah punya akun, login disini..</span>
            <p class="text-center"> <a href="{{ route('login') }}"> LOGIN </a> </p>
        </div>
    </div>
</div>

@endsection