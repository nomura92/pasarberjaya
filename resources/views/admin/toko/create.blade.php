@extends('admin.base')

@section('main')
<div class="container mt-3 mb-3">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Home</a></li>
	    <li class="breadcrumb-item"><a href="{{ route('toko.index') }}">Toko</a></li>
	    <li class="breadcrumb-item active" aria-current="page">Baru</li>
	  </ol>
	</nav>

	<div class="row">
		<div class="col-md-12">
			@if($errors->count() > 0)
                <div class="alert alert-warning" role="alert">
                @foreach($errors->all() as $item)
                	<p>{{ $item }}</p>
                @endforeach
                </div>
            @endif
			<form method="POST" action="{{ route('toko.store') }}" enctype="multipart/form-data">
			@csrf
				<div class="card mb-3">
					<div class="card-header">Profil Pengelola</div>
					<div class="card-body">
						<div class="form-row">
							<div class="form-group col-md-6">
								<label for="namap">Email Pengelola</label>
								<input class="form-control" type="email" name="email" id="email" value="{{ old('email') }}" required>
							</div>
							<div class="form-group col-md-6">
								<label for="namap">Nama Pengelola</label>
								<input class="form-control" type="text" name="namap" id="namap" value="{{ old('namap') }}" required>
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-6">
								<label for="kontakp">Kontak Pengelola</label>
								<input class="form-control" type="text" name="kontakp" id="kontakp" value="{{ old('kontakp') }}" onkeyup="nospaces(this)" required>
							</div>
							<!-- <div class="form-group col-md-6">
								<label for="alamatp">Alamat Pengelola</label>
								<input class="form-control" type="text" name="alamatp" id="alamatp" value="{{ old('alamatp') }}" required>
							</div> -->
						</div>
					</div>
				</div>
				<div class="card mb-3">
					<div class="card-header">Profil Toko</div>
					<div class="card-body">
						<div class="form-row">
							<div class="form-group col-md-4">
								<label for="namat">Nama Toko</label>
								<input class="form-control" type="text" name="namat" id="namat" value="{{ old('namat') }}" required>
							</div>
							<div class="form-group col-md-4">
								<label for="seo">Url / Seo Toko</label>
								<input class="form-control" type="text" name="seo_toko" id="seo" value="{{ old('seo') }}" required>
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-4">
								<label for="kontak">Provinsi</label>
								<select class="form-control" id="provinsi" name="provinsi" required>
									<option value="">Pilih Provinsi</option>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label for="kontak">Kabupaten/Kota</label>
								<select class="form-control" id="kota" name="kota" required>
									<option value="">Pilih Kabupaten/Kota</option>
								</select>
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-4">
								<label for="kontak">Kontak Toko</label>
								<input class="form-control" type="text" name="kontak" id="kontak" value="{{ old('kontak') }}" onkeyup="nospaces(this)" required>
							</div>
							<div class="form-group col-md-4">
								<label for="alamat">Alamat Toko</label>
								<input class="form-control" type="text" name="alamat" id="alamat" value="{{ old('alamat') }}" required>
							</div>
						</div>
					</div>
				</div>
				<div class="card mb-3">
					<div class="card-header">Akun Pengelola</div>
					<div class="card-body">
						<div class="form-row">
							<div class="form-group col-md-4">
								<label for="username">Username</label>
								<input class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" type="text" name="username" id="username" value="{{ $errors->has('username') ? '' : old('username') }}" minlength="4" onkeyup="nospaces(this)" required>
							</div>
							<div class="form-group col-md-4">
								<label for="password">Password</label>
								<input class="form-control" type="password" name="password" id="password" minlength="8" onkeyup="nospaces(this)" required>
							</div>
							<div class="form-group col-md-4">
								<label for="passwordRe">Konfirmasi Password</label>
								<input class="form-control" type="password" name="password_confirmation" id="password_confirmation" onkeyup="nospaces(this)" minlength="8" required>
							</div>
						</div>
					</div>
				</div>
				<div class="card mb-3">
					<div class="card-header">Aktifkan</div>
					<div class="card-body">
						<div class="form-row">
							<div class="form-group col-md-4">
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="1" name="aktifkan" id="aktifkan">
									<label class="form-check-label" for="aktifkan">Aktifkan</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				<button type="submit" class="btn btn-sm btn-success">Simpan</button>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
function nospaces(t){
  if(t.value.match(/\s/g)){
    t.value=t.value.replace(/\s/g,'');
  }
}
</script>
@endsection

@section('js')
<script type="text/javascript">
	var provinsi = {!! $provinsi !!};
	var vkota = {!! $kota !!};

	function prov(array){
		$.each(array, function(key, value) {   
		     $('#provinsi').append($("<option>").attr("value",value.id_provinsi).text(value.nm_provinsi)); 
		});
	}

	function kota(array, provinsi_id){
		$('#kota').find('option').not(':first').remove();
		var kota = array.filter(item => String(item.id_provinsi).toLowerCase().includes(provinsi_id));
		for(var i in kota){
		     $('#kota').append($("<option>").attr("value",kota[i].id_kota).text(kota[i].nm_kota));
		     console.log(kota[i]);
		}
	}

	$(document).on('change', '#provinsi', function(e){
		var value = $(this).val();
		kota(vkota, value);
	});

	prov(provinsi);

	function convertToSlug(Text)
	{
	    return Text
	        .toLowerCase()
	        .replace(/[^\w ]+/g,'')
	        .replace(/ +/g,'-')
	        ;
	}
	$(document).on('blur', '#namat', function(e){
		var text = $(this).val();

		$('#seo').val(convertToSlug(text));
	});
</script>
@endsection