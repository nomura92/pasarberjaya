@extends('admin.base')

@section('main')

<div class="container mt-3 mb-3">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item"><a href="{{ route('beranda.index') }}">Home</a></li>
	    <li class="breadcrumb-item active" aria-current="page">{{ ucfirst(request()->segment(2)) }}</li>
	  </ol>
	</nav>

	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					<div class="table-responsive">
						<a href="{{ route(request()->segment(2).'.create') }}" class="btn btn-primary btn-sm mb-3">{{ ucfirst(request()->segment(2)) }} Baru</a>
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Judul</th>
									<th>Gambar</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
							@foreach($data as $item)
								<tr>
									<td>{{ $item->title }}</td>
									<td><img src="{{ asset($item->gambar) }}" width="100"></td>
									<td>
										<a href="{{ route(request()->segment(2).'.edit',$item->id) }}" class="btn btn-primary btn-sm">Edit</a>
										<a href="{{ route(request()->segment(2).'.destroy',$item->id) }}" class="btn btn-danger btn-sm btn-hapus">Hapus</a>
										<form action="{{ route(request()->segment(2).'.destroy',$item->id) }}" method="post">
										@csrf
										@method('DELETE')
										</form>
									</td>
								</tr>
							@endforeach
							</tbody>
						</table>
					</div>
					{{ $data->links() }}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection