@extends('referral.base')

@section('title', 'Dashboard Referral')
@section('refhome', 'active')

@section('main')
<div class="container mt-3">
	<div class="card mb-3">
		<div class="card-body">
			<table cellpadding="10">
				<tr>
					<td>Nama Toko</td>
					<td>:</td>
					<td>{{ $toko->nm_toko }}</td>
				</tr>
				<tr>
					<td>Alamat Toko</td>
					<td>:</td>
					<td>{{ $toko->alamat_toko }}</td>
				</tr>
				<tr>
					<td>Telp Toko</td>
					<td>:</td>
					<td>{{ $toko->no_telp_toko }}</td>
				</tr>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="card mb-3">
				<div class="card-body">
					<span>Total Member</span>
					<h3>{{ $member->count() }}</h3>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="card mb-3">
				<div class="card-body">
					<span>Total Transaksi</span>
					<h3>{{ rupiah($order->total) }}</h3>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="card mb-3">
				<div class="card-body">
					<span>Perkiraan Komisi</span>
					<h3>{{ rupiah($order->total*0.02) }}</h3>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection