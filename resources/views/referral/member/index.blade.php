@extends('referral.base')

@section('title', 'Member')
@section('refmember', 'active')

@section('main')
<div class="container mt-3">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">Data Member Referral Anda</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Nama Lengkap</th>
									<th>Tanggal Terdaftar</th>
									<th>Total Belanja</th>
								</tr>
							</thead>
							<tbody>
							@forelse($member as $row)
								<tr>
									<td>{{ $row->nama }}</td>
									<td>{{ $row->created_at }}</td>
									<td class="text-right">{{ rupiah($order->where('id_member', $row->id_member)->sum('total')) }}</td>
								</tr>
							@empty
								<tr>
									<td colspan="3" class="text-center">Belum ada data</td>
								</tr>
							@endforelse
							</tbody>
						</table>
					</div>
					{{ $member->links() }}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection