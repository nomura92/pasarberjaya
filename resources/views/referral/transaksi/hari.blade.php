@extends('referral.base')

@section('title', 'Transaksi')
@section('reftransaksi', 'active')

@section('main')
<div class="container mt-3">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">Data Transaksi Member Tanggal {{ $tanggal }}</div>
				<div class="card-body">
					<a href="{{ url()->previous() }}" class="btn btn-warning mb-3">Kembali</a>
					<div class="table-responsive">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Member</th>
									<th>Total Belanja</th>
									<th>Perkiraan Komisi</th>
								</tr>
							</thead>
							<tbody>
							@forelse($transaksi as $row)
								<tr>
									<td>{{ $row->nama }}</td>
									<td class="text-right">{{ rupiah($row->total) }}</td>
									<td class="text-right">{{ rupiah($row->total*0.02) }}</td>
								</tr>
							@empty
								<tr>
									<td colspan="3" class="text-center">Belum ada data</td>
								</tr>
							@endforelse
							</tbody>
							<tfoot>
								<tr>
									<th>TOTAL</th>
									<th class="text-right">{{ rupiah($transaksi->sum('total')) }}</th>
									<th class="text-right">{{ rupiah($transaksi->sum('total')*0.02) }}</th>
								</tr>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection