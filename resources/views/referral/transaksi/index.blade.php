@extends('referral.base')

@section('title', 'Transaksi')
@section('reftransaksi', 'active')

@section('main')
<div class="container mt-3">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">Data Transaksi Member Bulan {{ bulan($bulan) }}-{{ $tahun }}</div>
				<div class="card-body">
					<form method="get" action="" class="form-inline float-right mb-3">
						<div class="input-group">
							<select class="form-control" name="bulan" required>
								<option value="">-Pilih Bulan-</option>
								@for($bln=1;$bln<=12;$bln++)
									@if($bln==$bulan)
									<option value="{{ $bln }}" selected>{{ bulan($bln) }}</option>
									@else
									<option value="{{ $bln }}">{{ bulan($bln) }}</option>
									@endif
								@endfor
							</select>
							<select class="form-control" name="tahun" required>
								<option value="">-Pilih Tahun-</option>
								@for($thn=2019;$thn<=date('Y');$thn++)
									@if($thn==$tahun)
									<option value="{{ $thn }}" selected>{{ $thn }}</option>
									@else
									<option value="{{ $thn }}">{{ $thn }}</option>
									@endif
								@endfor
							</select>
							<div class="input-group-prepend">
								<button type="submit" class="btn btn-outline-primary">Telusuri</button>
							</div>
						</div>
					</form>
					<div class="table-responsive">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Tanggal</th>
									<th>Total Belanja</th>
									<th>Perkiraan Komisi</th>
								</tr>
							</thead>
							<tbody>
							@forelse($transaksi as $row)
								<tr>
									<td><a href="{{ route('ref.transaksihari',$row->date) }}">{{ $row->date }}</a></td>
									<td class="text-right">{{ rupiah($row->total) }}</td>
									<td class="text-right">{{ rupiah($row->total*0.02) }}</td>
								</tr>
							@empty
								<tr>
									<td colspan="3" class="text-center">Belum ada data</td>
								</tr>
							@endforelse
							</tbody>
							<tfoot>
								<tr>
									<th>TOTAL</th>
									<th class="text-right">{{ rupiah($transaksi->sum('total')) }}</th>
									<th class="text-right">{{ rupiah($transaksi->sum('total')*0.02) }}</th>
								</tr>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection