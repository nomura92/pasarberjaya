@extends('referral.auth.base')

@section('title', 'Login Referral')

@section('main')
	<div class="">
		<div class="container d-flex justify-content-center">
			<div class="row">
				<div class="card">
					<div class="card-header">
						<center><strong>Pengusaha Mitra Pasarpedia</strong></center>
					</div>
					<div class="card-body">
						<form name="login" id="login" method="post" action="{{ route('ref.login') }}">
						@csrf
							<div class="row">
								<div class="col">
									<span class="profile-img">
										<i class='fas fa-user-circle' style='font-size:120px'></i>
									</span>
								</div>
							</div>
							<div class="row">
								<div class="col">
									<hr> <!-- other content  -->
								</div>
							</div>
							<div class="row">
								<div class="col">
									<div class="form-group">
										<div class="input-group mb-3">
											<div class="input-group-prepend">
												<span class="input-group-text" id="basic-addon1">
													<i class='fas fa-user-shield'></i>
												</span>
											</div>
											<input class="form-control" placeholder="Kode PMP" id="username" name="username" type="text" autofocus>
										</div>
									</div>
									<div class="form-group">
										<div class="input-group mb-3">
											<div class="input-group-prepend">
												<span class="input-group-text" id="basic-addon1">
													<i class='fas fa-user-secret'></i>
												</span>
											</div>
											<input class="form-control" placeholder="Password" id="loginPassword" name="password" type="password">
										</div>
									</div>
									<div class="form-group">
										<input type="submit" class="btn btn-sm btn-success btn-block submit" id="login_m"  value="Login">
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection