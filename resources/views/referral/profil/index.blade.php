@extends('referral.base')

@section('title', 'Profil')

@section('main')
<div class="container mt-3">
	<div class="card mb-3">
		<div class="card-header">Data Bank</div>
		<div class="card-body">
			<form method="POST" action="{{ route('ref.profilUpdateBank') }}" enctype="multipart/form-data">
				@csrf
				@method('PUT')
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="bank">Bank</label>
						<select class="form-control" name="bank" id="bank" required>
							<option value="">Pilih</option>
							@foreach($bank as $item)
								@if($item->id_bank==$profil->id_bank)
								<option value="{{ $item->id_bank }}" selected>{{ $item->nama }}</option>
								@else
								<option value="{{ $item->id_bank }}">{{ $item->nama }}</option>
								@endif
							@endforeach
						</select>
					</div>
					<div class="form-group col-md-6">
						<label for="nama">Atas Nama</label>
						<input type="text" name="nama" id="nama" class="form-control" value="{{ $profil->atasnama }}" required>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="norek">Nomor Rekening</label>
						<input type="text" name="norek" id="norek" class="form-control" value="{{ $profil->norek }}" required>
					</div>
				</div>
				<button type="submit" class="btn btn-sm btn-success">Simpan</button>
			</form>
		</div>
	</div>
	<div class="card mb-3">
		<div class="card-header">Ganti Password</div>
		<div class="card-body">
			<form method="POST" action="{{ route('ref.profilUpdatePass') }}" enctype="multipart/form-data">
				@csrf
				@method('PUT')
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="password">Password</label>
						<input type="password" name="password" id="password" class="form-control" required>
					</div>
					<div class="form-group col-md-6">
						<label for="password_confirmation">Konfirmasi Password</label>
						<input type="password" name="password_confirmation" id="password_confirmation" class="form-control" required>
					</div>
				</div>
				<button type="submit" class="btn btn-sm btn-success">Simpan</button>
			</form>
		</div>
	</div>
</div>
@endsection