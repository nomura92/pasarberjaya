@extends('apk.base')

@section('main')

<ul class="list-group">
  <li class="list-group-item bg-white mb-3 rounded-0">
  	<h5 class="card-title">{{ Auth::user()->nama }}</h5>
  	<span>{{ Auth::user()->nohp }}</span>
  </li>
  
  <li class="list-group-item bg-white mb-3 rounded-0"><a href="{{ route('apk.profil') }}">Profil Anda</a></li>
  
  <li class="list-group-item bg-white rounded-0">
	<a  href="{{ route('logout') }}">Logout</a>
  </li>
</ul>

@endsection