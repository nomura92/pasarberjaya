@extends('apk.base')

@section('main')

<div class="mb-3 pt-3">
	<div class="container">
		<a href="#" id="cFSearch"><i class="fa fa-times"></i></a>
		<div id="fSearch">
			<form method="get" action="">
				<input type="search" id="kota" class="form-control form-control-lg" placeholder="Cari Pasar di Kota-mu" value="@php if(isset($_GET['kota'])) echo $kota->where('id', $_GET['kota'])->first()['kota']; @endphp" autocomplete="off">
				<input type="hidden" name="kota" id="kota-id">
			</form>
			<div id="fSOption">
				<ul class="list-group">
				</ul>
			</div>
		</div>
	</div>
</div>

<div class="container mt-3">
    
	@if(Request()->get('kota'))
	<div class="row mb-3">
		@forelse($allToko as $v)
		<div class="col-6 text-center">
			<div class="chatbox">
				@if(ENV('APP_ENV') == 'local')
				<div class="bodi cpointer" onclick="window.location.href='{{ url('u/'.$v->seo_toko) }}'">
				@else
				<div class="bodi cpointer" onclick="window.location.href='http://{{ $v->seo_toko }}.pasarberjaya.com'">
				@endif
					<span>
						<h5 class="namaProduk">{{ $v->nm_toko }}</h5>
						<img class="w-50" src="{{ asset('img/supermarket-icon-png-12837.png') }}">
					</span>
				</div>
			</div>
			<h5>{{ $kota->where('id', $v->id_kota)->first()['kota'] }}</h5>
		</div>	
		@empty
		<div class="container pt-1">
		<div class="chatbox">
            <div class="bodi">
            <span class="tip tip-left"></span>
                <span><img src="{{ asset('img/sad.png') }}"> Maaf kak belum ada pasar disini, Secepatnya kami akan buka di Kota ini..</span>
            </div>
        </div>
        </div>
		@endforelse
	</div>
	@endif
	@if(!Request()->get('kota'))
	
    <div class="container pt-3 pb-3">
        <div class="chatbox">
            <div class="bodi">
            <span class="tip tip-left"></span>
                <span><img src="{{ asset('img/smile.png') }}"> Yuk belanja.. pilih pasar dibawah <i class="far fa-hand-point-down"></i></span>
            </div>
        </div>
    </div>	
    <div class="row">
	@foreach($allToko as $k => $v)
		<div class="col-6 text-center mb-3">
			<div class="chatbox">
				@if(ENV('APP_ENV') == 'local')
				<div class="bodi cpointer" onclick="window.location.href='{{ url('u/'.$v->seo_toko) }}'">
				@else
				<div class="bodi cpointer" onclick="window.location.href='http://{{ $v->seo_toko }}.pasarberjaya.com'">
				@endif
					<span>
						<h6 class="namaProduk"><b>{{ $v->nm_toko }}</b></h6>
						<img style="width:100px" src="{{ asset('img/supermarket-icon-png-12837.png') }}">
					</span>
				</div>
			</div>
			<h6>{{ $v->nm_toko }}</h6>
		</div>
	@endforeach
	</div>
	@endif
</div>

<div class="container mb-3">
	
	<hr style="border-color: #ccc">

	<div>
		<div class="sliders" style="width: 100%;">
			@foreach($slides as $k => $v)
			<div><img src="{{ asset($v->gambar) }}" style="width: 100%;"></div>
			@endforeach
		</div>
	</div>

	<hr style="border-color: #ccc">

	<div>
		<h5>PROMO</h5>
		<div class="sliders" style="width: 100%;">
			@foreach($promo as $k => $v)
			<div data-href="{{ route('apk.detail', $v->id) }}" class="slide-link">
				<img src="{{ asset($v->gambar) }}" style="width: 100%;">
				<p>{{ $v->title }}</p>
				<p>{!! substr(strip_tags($v->description), 0, 75) !!}</p>
			</div>
			@endforeach
		</div>
	</div>

	<hr style="border-color: #ccc">

	<div>
		<h5>INFORMASI</h5>
		<div class="sliders" style="width: 100%;">
			@foreach($information as $k => $v)
			<div data-href="{{ route('apk.detail', $v->id) }}" class="slide-link">
				<img src="{{ asset($v->gambar) }}" style="width: 100%;">
				<p>{{ $v->title }}</p>
				<p>{!! substr(strip_tags($v->description), 0, 75) !!}</p>
			</div>
			@endforeach
		</div>
	</div>
</div>

@foreach($allToko as $k => $v)
<div class="container mb-3">
	<div class="product-selection mb-3">
		<h6 ><mark style="background-color: #a1d2f9; padding-right: 15px; border-radius: 10px 60px 0 10px;">{{ $v->nm_toko }}</mark></h6>
		<!-- Slider main container -->
		<div class="slide2 swiper-container">
		    <!-- Additional required wrapper -->
		    <div class="swiper-wrapper">
		        <!-- Slides -->
		        @foreach($v->produk->take(6) as $item)
		        <div class="swiper-slide">
		        	<a href="{{ route('index', $v->seo_toko) }}">
				        @if($item->gambar!='')
				        <img class="img-fluid" src="{{ asset('img/produk/'.$item->gambar) }}">
				        @else
				        <img class="img-fluid" src="https://via.placeholder.com/350x250.png?text=Image+not+found">
				        @endif
				        @if($item->diskon > 0)
						<span class="diskon">{{ round(100 - (($item->harga-$item->diskon)/$item->harga*100)) }}%</span>
						@endif
				        <div class="p-2">
					        <h5 class="namaProduk">{{ $item->nm_produk }}</h5>
					        <div>
					        	
					        	@if($item->diskon > 0)
					        	<span class="harga lt">{{ rupiah(($item->harga)) }} /<small>{{ $item->satuan }}</small></span><br>
					        	<span>{{ rupiah($item->harga-$item->diskon) }}</span>
					        	@else
					        	<span class="harga">{{ rupiah(($item->harga)) }} /<small>{{ $item->satuan }}</small></span><br>
					        	@endif
					        </div>
				        </div>
				    </a>
			    </div>
				@endforeach
		    </div>
		</div>
	</div>
</div>
<hr>
@endforeach

@endsection

@push('scripts')
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script type="text/javascript">
    var kota = {!! $kota !!};

    $('#kota').on({
        focus : function(e){
            $('#fSearch').parent().parent().addClass('FSactive');
            var value = $(this).val();
            Filter(kota, value);
        },
        keyup: function(e){
            var value = $(this).val();
            Filter(kota, value);
            $('#kota-id').val('');
        }
    });
    $(document).on('click', '.selectOption',function(e){
        var opt = $(this).data('id');
        var optKota = $(this).data('kota');
        $('#kota').val(optKota);
        $('#kota-id').val(opt);
        $('#kota').focus();
    });
    $('#cFSearch').on('click',function(e){
    	e.preventDefault();
        $('#fSearch').parent().parent().removeClass('FSactive');
        $('#kota').val('');
        $('#fSOption ul').empty();
    });

    function Filter(array, value){
        if(value.length > 0){
            $('#fSOption ul').empty();
            const filterKota = array.filter( zakar => String(zakar.provinsi).toLowerCase().includes(String(value).toLowerCase()) || String(zakar.kota).toLowerCase().includes(String(value).toLowerCase()));

            //console.log(filterKota);
            for(var i in filterKota){
                $('#fSOption ul').append('<li class="list-group-item selectOption" data-id="'+filterKota[i].id+'" data-kota="'+filterKota[i].kota+'">'+filterKota[i].kota+'</li>');
            }

        }else{
            $('#fSOption ul').empty();
        }
    }

    $(document).ready(function(){
    	$('.sliders').slick({
    		arrows: false,
    		autoplay: true,
  			autoplaySpeed: 2000,
    	});
    	$(document).on('click', '.slide-link', function(e){
    		var href = $(this).data('href');

    		window.location.href = href;
    	});
    });
</script>
@endpush