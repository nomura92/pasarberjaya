@extends('apk.base')

@section('main')

<div class="container pt-3 pb-3">
    <div class="chatbox">
        <div class="bodi">
        <span class="tip tip-left"></span>
            <span><img src="{{ asset('img/smile.png') }}" style="width:25px"> Terimakasih atas transaksinya, CS kami akan segera menghubungi.. <br>atau bisa Chat kesini :</span>
            <a href="https://api.whatsapp.com/send?phone={{ $toko->call_center }}&amp;text=Halo.." target="_blank"> <h6 style="color:#f6b20e; font-size:18px" class="text-center pt-3"><img src="https://pasarpedia.id/img/waicon.png"> Whatsapp </h6>
  		    </a>
        </div>
    </div>
</div>

<?php
	switch($order->status){
		case 'pending':
			$status = 'warning';
		break;
		case 'proses':
			$status = 'primary';
		break;
		case 'sukses':
			$status = 'success';
		break;
		case 'batal':
			$status = 'danger';
		break;
	}
?>

<div class="container pt-3 pb-3">
    <div class="report">
        <div class="isi">
            <ul class="list-group">
	            <li class="list-group-item rounded-0"><b>Toko</b><br>{{ $order->toko->nm_toko }}</li>
	            <li class="list-group-item rounded-0"><b>Status</b><br><span class="btn-{{ $status }} btn">{{ ucfirst($order->status) }}</span></li>
	            <li class="list-group-item rounded-0">Nomor Pesanan<br>{{ nopesan(Auth::user()->id_toko,$order->id_order,$order->created_at) }}</li>
	            <li class="list-group-item rounded-0">Tanggal Pemesanan<br>{{ $order->created_at }}</li>
            </ul>
        </div>
    </div>
</div>

<div class="container pt-3 pb-3">
    <div class="report">
        <div class="isi">
            <ul class="list-group">
	            <li class="list-group-item rounded-0"><b>Payment</b></li>
	            <li class="list-group-item rounded-0">
		            Total Pembayaran
		            <span class="float-right">{{ $order->getPitih($order->total) }}</span>
	            </li>
	            <li class="list-group-item rounded-0">
		            Metode Pembayaran
		            <span class="float-right">{{ $metode ? $metode->name : ucfirst($order->metode) }}</span>
	            </li>
	            <li class="list-group-item rounded-0">
		            Tipe Pembayaran
		            <span class="float-right">Cash</span>
	            </li>
	            @if($order->note)
	            <li class="list-group-item rounded-0">
	                <h5>Note :</h5>
	                <p>{{ $order->note }}</p>
	            </li>
	            @endif
            </ul>
        </div>
    </div>
</div>

@if($order->status == 'pending' && strpos($order->metode, 'tf') !== false)
@if($metode)
<div class="container pt-3 pb-3">
    <div class="report">
        <div class="isi">
            <ul class="list-group">
	            <li class="list-group-item rounded-0"><b>Silahkan transfer pada bank dibawah ini :</b></li>
				<li class="list-group-item rounded-0">
					<div class="mb-1">
						<span>{{ $metode->name }}</span><br>
						<span>Nama Bank :<b> {{ $metode->bank()->bank->nama }} </b></span><br>
						<span>Rekening :<b> {{ $metode->bank()->norek }} </b></span><br>
						<span>Atas nama : {{ $metode->bank()->atas_nama }}</span>
					</div>
				</li>
            </ul>
        </div>
    </div>
</div>
@endif
@endif

<div class="container pt-3 pb-3">
    <div class="report">
        <div class="isi">
            <ul class="list-group">
	            <li class="list-group-item rounded-0"><b>Alamat Pengiriman</b></li>
	            <li class="list-group-item rounded-0">{{ $order->nama }}<br>{{ $order->alamat }}</li>
            </ul>
        </div>
    </div>
</div>

<div class="container pt-3 pb-5">
    <div class="chatbox">
        <div class="bodi">
        <span class="tip tip-left"></span>
            <span><img src="{{ asset('img/smile.png') }}" style="width:25px"> Kalo kakak ordernya diatas jam 15:00, barang kami antar besoknya ya kak mulai jam 09:00 pagi. </span>
        </div>
    </div>
</div>

<ul class="list-group bg-white mb-3">
	<li class="list-group-item rounded-0"><b>Daftar Barang</b></li>
	<?php $total = $diskon = 0; ?>
	@foreach($order->detail($order->id_order) as $item)
	<li class="list-group-item rounded-0 clearfix">
		<div class="media">
			@if($item->gambar!='')
			<img class="media-left" width="80" src="{{ asset('img/produk/'.$item->gambar) }}">
			@else
			<img class="media-left" width="80" src="https://via.placeholder.com/350x150.png?text=IMAGE NOT FOUND">
			@endif
			<div class="media-body pl-3">
				<h6 class="card-title">{{$item->nm_produk}}</h6>
				<p class="card-text">
					{{ rupiah($item->harga) }} / <small>{{ $item->satuan }}</small> <br>
					Jumlah {{ $item->jumlah }}<br>
					{{ rupiah($item->harga*$item->jumlah) }}
				</p>
			</div>
		</div>
	</li>
	<?php $total = $total + ($item->jumlah * $item->harga); $diskon = $diskon + $item->diskon; ?>
	@endforeach
</ul>

<ul class="list-group bg-white mb-3">
	<li class="list-group-item rounded-0">
		<b>Jumlah Pembelian</b>
		<span class="float-right">{{ $order->jumlahItem($order->id_order)->count() }}</span>
	</li>
	<li class="list-group-item rounded-0">
		<b>Total Belanja</b>
		<span class="float-right">{{ rupiah($total) }}</span>
	</li>
	<li class="list-group-item rounded-0">
		<b>Diskon</b>
		<span class="float-right">{{ rupiah($diskon) }}</span>
	</li>
	<li class="list-group-item rounded-0">
		<b>Jasa belanja & Antar</b>
		<span class="float-right">{{ rupiah($order->total - ($total)) }}</span>
	</li>
	<li class="list-group-item rounded-0">
		<b>Total Bayar</b>
		<span class="float-right">{{ rupiah($order->total) }}</span>
	</li>
</ul>

@endsection