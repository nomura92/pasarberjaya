@extends('apk.base')

@section('main')

<div class="container">
		<div class="card-body">
			<form method="POST" action="{{ route('login') }}">
				@csrf
				<div class="form-group">
					<label for="nohp">Nomor Whatsapp</label>
					<input class="form-control" type="number" name="nohp" id="nohp" value="{{ old('nohp') }}" required autofocus>
					@if ($errors->has('nohp'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('nohp') }}</strong>
                        </span>
                    @endif
				</div>
				<div class="form-group">
					<label for="password">Password</label>
					<input class="form-control" type="password" name="password" id="password" required>
					@if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
				</div>
				<div class="form-group">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" checked>
                        <label class="form-check-label" for="remember">Ingat saya</label>
                    </div>
                </div>
                <button class="btn btn-primary" type="submit">Login</button>
			</form>
		</div>
</div>

<div class="container pt-5 pb-3">
    <div class="chatbox">
        <div class="bodi">
        <span class="tip tip-left"></span>
            <span><img src="{{ asset('img/smile.png')}}"> kalau belum punya akun daftar dulu kak..</span>
            <p class="text-center"> <a href="{{ route('register') }}"> <b>DAFTAR</b> </a> </p>
        </div>
    </div>
</div>

<div class="container pt-5 pb-3">
    <div class="chatbox">
        <div class="bodi">
        <span class="tip tip-left"></span>
            <span><img src="{{ asset('img/smile.png')}}"> lupa password ?.. chat ke admin aja kak</span>
  		    </a>
        </div>
    </div>
</div>

@endsection