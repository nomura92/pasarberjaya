@extends('apk.base')

@section('main')
<div class="card rounded-0 mb-3">
	<img class="card-img-top" src="{{ asset($data->gambar) }}" alt="{{ $data->title }}">
	<div class="card-body">
		<h5 class="card-title">{{ $data->title }}</h5>
		<span class="text-muted">{{ tgl_indo($data->created_at) }}</span><br>
		{!! $data->description  !!}
	</div>
</div>
@endsection