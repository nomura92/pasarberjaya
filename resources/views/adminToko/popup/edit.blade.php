@extends('adminToko.base')

@section('main')

<div class="container mt-3 mb-3">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Home</a></li>
	    <li class="breadcrumb-item"><a href="{{ route('popup.index') }}">Bank</a></li>
	    <li class="breadcrumb-item active" aria-current="page">Baru</li>
	  </ol>
	</nav>

	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					<form method="POST" action="{{ route('popup.update', $popup->id) }}" enctype="multipart/form-data">
						@csrf
						@method('PUT')
						<div class="form-row">
							<div class="form-group col-md-6">
								<label for="title">Header / Title</label>
								<input type="text" name="title" id="title" class="form-control" value="{{ $popup->header }}" required>
							</div>
							<div class="form-group col-md-6">
								<label for="image">Image</label>
								<input type="file" name="image" id="image" class="form-control">
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-12">
								<label for="description">Description</label>
								<textarea name="description" id="description" class="form-control summernote">{{ $popup->description }}</textarea>
							</div>
						</div>
						<button type="submit" class="btn btn-sm btn-success">Simpan</button>
					</form>
					<p class="mt-3">Image bisa diisi atau tidak.</p>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection