@extends('adminToko.base')

@section('main')

<div class="container mt-3 mb-3">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Home</a></li>
	    <li class="breadcrumb-item active" aria-current="page">Popup</li>
	  </ol>
	</nav>

	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					<div class="table-responsive">
						<a href="{{ route('popup.create') }}" class="btn btn-primary btn-sm mb-3">Popup Baru</a>
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Header</th>
									<th>Image</th>
									<th>Description</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
							@forelse($popup as $item)
								<tr>
									<td>{{ $item->header }}</td>
									<td>{{ $item->image ? $item->image : '-' }}</td>
									<td>{{ substr(strip_tags($item->description), 0 , 75) }}</td>
									<td>
										@if($item->tampil > 0)
										<a href="{{ route('popup.show',$item->id) }}?tampil=0" class="btn btn-success btn-sm" onclick="return confirm('Jadikan popup hidden?')">Publish</a>
										@else
										<a href="{{ route('popup.show',$item->id) }}?tampil=1" class="btn btn-secondary btn-sm" onclick="return confirm('Jadikan popup tampil?')">Unpublish</a>
										@endif
										<a href="{{ route('popup.edit',$item->id) }}" class="btn btn-primary btn-sm">Edit</a>
										<a href="{{ route('popup.destroy',$item->id) }}" class="btn btn-danger btn-sm btn-hapus">Hapus</a>
										<form action="{{ route('popup.destroy',$item->id) }}" method="post">
										@csrf
										@method('DELETE')
										</form>
									</td>
								</tr>
							@empty
								<tr>
									<td colspan="4" class="bg-info text-white text-center">Empty (0) Data</td>
								</tr>
							@endforelse
							</tbody>
						</table>
					</div>
					{{ $popup->links() }}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection