@extends('adminToko.base')

@section('main')

<div class="container mt-3 mb-3">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Home</a></li>
	    <li class="breadcrumb-item active" aria-current="page">News</li>
	  </ol>
	</nav>

	@if(session()->get('success'))
    <div class="alert alert-success">
    	{{ session()->get('success') }}  
	</div>
    @endif

	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					<div class="table-responsive">
						<a href="{{ route('news.create') }}" class="btn btn-primary btn-sm mb-3" data-toggle="tooltip" title="Tambah news baru">News Baru <i class="fa fa-plus"></i></a>
						<h6><font size="2">* Fitur notifikasi ke member, untuk menyampaikan news, promo, diskon dll.</font></h6>
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Tanggal Dibuat</th>
									<th>Judul</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
							@forelse($newsToko as $item)
								<tr>
									<td>{{ tgl_indo($item->created_at) }}</td>
									<td>{{ $item->judul }}</td>
									<td>
										<a href="{{ route('news.show',$item->id_news) }}" data-toggle="tooltip" class="btn btn-info btn-sm" title="Detail news"><i class="fa fa-search"></i></a>
									</td>
								</tr>
							@empty
								<tr>
									<td colspan="4" align="center">Belum ada data</td>
								</tr>
							@endforelse
							</tbody>
						</table>
					</div>
					{{ $newsToko->links() }}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection