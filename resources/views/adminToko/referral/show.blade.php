@extends('adminToko.base')

@section('main')

<div class="container mt-3 mb-3">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Home</a></li>
	    <li class="breadcrumb-item"><a href="{{ route('referral.index') }}">Referral</a></li>
	    <li class="breadcrumb-item active" aria-current="page">Member</li>
	  </ol>
	</nav>

	<div class="row mb-3">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">Komisi Referral</div>
				<div class="card-body">
					<form method="get" action="" class="form-inline float-right mb-3">
						<div class="input-group">
							<select class="form-control" name="bulan" required>
								<option value="">-Pilih Bulan-</option>
								@for($bln=1;$bln<=12;$bln++)
									@if($bln==$bulan)
									<option value="{{ $bln }}" selected>{{ bulan($bln) }}</option>
									@else
									<option value="{{ $bln }}">{{ bulan($bln) }}</option>
									@endif
								@endfor
							</select>
							<select class="form-control" name="tahun" required>
								<option value="">-Pilih Tahun-</option>
								@for($thn=2019;$thn<=date('Y');$thn++)
									@if($thn==$tahun)
									<option value="{{ $thn }}" selected>{{ $thn }}</option>
									@else
									<option value="{{ $thn }}">{{ $thn }}</option>
									@endif
								@endfor
							</select>
							<div class="input-group-prepend">
								<button type="submit" class="btn btn-outline-primary">Telusuri</button>
							</div>
						</div>
					</form>
					<div class="table-responsive">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Tanggal</th>
									<th>Jumlah Transaksi</th>
									<th>Komisi</th>
								</tr>
							</thead>
							<tbody>
							@forelse($transaksi as $row)
								<tr>
									<td><a href="{{ route('ref.transaksihari',$row->date) }}">{{ $row->date }}</a></td>
									<td class="text-right">{{ rupiah($row->total) }}</td>
									<td class="text-right">{{ rupiah($row->total*0.02) }}</td>
								</tr>
							@empty
								<tr>
									<td colspan="3" class="text-center">Belum ada data</td>
								</tr>
							@endforelse
							</tbody>
							<tfoot>
								<tr>
									<th>TOTAL</th>
									<th class="text-right">{{ rupiah($transaksi->sum('total')) }}</th>
									<th class="text-right">{{ rupiah($transaksi->sum('total')*0.02) }}</th>
								</tr>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">Member Referral</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Nama</th>
									<th>Nohp</th>
									<th>Total Belanja</th>
								</tr>
							</thead>
							<tbody>
							@foreach($member as $item)
								<tr>
									<td>{{ $item->nama }}</td>
									<td>{{ $item->nohp }}</td>
									<td class="text-right">{{ rupiah($order->where('id_member', $item->id_member)->sum('total')) }}</td>
								</tr>
							@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection