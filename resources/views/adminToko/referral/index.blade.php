@extends('adminToko.base')

@section('main')

<div class="container mt-3 mb-3">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Home</a></li>
	    <li class="breadcrumb-item active" aria-current="page">Referral</li>
	  </ol>
	</nav>

	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					<div class="table-responsive">
						<a href="{{ route('referral.create') }}" class="btn btn-primary btn-sm mb-3">Referral Baru</a>
						<table class="table table-bordered datatable">
							<thead>
								<tr>
									<th>Kode</th>
									<th>Terdaftar</th>
									<th>Nama</th>
									<th>Bank</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
							@foreach($referral as $item)
								<tr>
									<td><a href="{{ route('referral.show', $item->id_ref) }}">{{ $item->kode }}</a></td>
									<td>{{ $item->created_at }}</td>
									<td>{{ $item->nama }}</td>
									<td>{{ $bank->where('id_bank',$item->id_bank)->first()['nama'] }} <br> {{ $item->norek }} <br> {{ $item->atasnama }}</td>
									<td>
										<a href="{{ route('referral.edit',$item->id_ref) }}" class="btn btn-primary btn-sm">Edit</a>
										<a href="{{ route('referral.destroy',$item->id_ref) }}" class="btn btn-danger btn-sm btn-hapus">Hapus</a>
										<form action="{{ route('referral.destroy',$item->id_ref) }}" method="post">
										@csrf
										@method('DELETE')
										</form>
									</td>
								</tr>
							@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection