@extends('adminToko.base')

@section('main')
<div class="container mt-3 mb-3">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Home</a></li>
	    <li class="breadcrumb-item"><a href="{{ route('orderan.index') }}">Orderan</a></li>
	    <li class="breadcrumb-item"><a href="{{ route('orderan.show', $order->id_order) }}">{{ $order->id_order }}</a></li>
	    <li class="breadcrumb-item active" aria-current="page">Tambah</li>
	  </ol>
	</nav>

    <div class="mt-3 card">
        <div class="card-body">
            <h4>Tambah Item baru</h4>
            <form action="{{ route('orderan.store') }}" method="POST">
            @csrf
            <input type="hidden" name="order" value="{{ $order->id_order }}">
                <table class="table table-bordered items">
                    <thead>
                        <tr>
                            <th>Produk</th>
                            <th>Harga</th>
                            <th>Jumlah</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td width="50%">
                                <select class="form-control produk" name="produk[]">
                                    <option value="">Pilih</option>
                                @foreach($produk as $k => $v)
                                    <option value="{{ $v->id_produk }}" data-harga="{{ rupiah($v->harga - $v->diskon) }}">{{ $v->nm_produk.' - '.$v->satuan }}</option>
                                @endforeach
                                </select>
                            </td>
                            <td class="text-right"></td>
                            <td width="100"><input type="number" class="form-control" name="jumlah[]" min="1" required></td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4" class="text-right"><a class="btn btn-info addItem"><i class="fa fa-plus"></i></a></td>
                        </tr>
                    </tfoot>
                </table>
                <button type="submit" class="btn btn-success">Simpan</button>
            </form>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script>
var data = {!! $produk->toJson() !!};
var cloneItem = $('.items > tbody > tr:first-child').html();
$(document).on('click', '.addItem', function(){
    $('.items tbody').append('<tr>'+cloneItem+'<td width="30"><a class="btn btn-danger text-white delete"><i class="fa fa-trash"></i></a></td></tr>');
    $('.items tbody tr:last-child').find('.produk').select2();
});
$(document).on('click', '.delete', function(){
    $(this).parent().parent().remove();
});
$(document).on('change', '.produk', function(){
    var harga = $(this).find(':selected').data('harga');
    $(this).parent().next().text(harga);
});
$(document).ready(function(){
    $('.produk').select2();
});
</script>
@endpush