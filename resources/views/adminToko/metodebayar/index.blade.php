@extends('adminToko.base')

@section('main')

<div class="container mt-3 mb-3">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Home</a></li>
	    <li class="breadcrumb-item active" aria-current="page">Metode</li>
	  </ol>
	</nav>

	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					<div class="table-responsive">
						<a href="{{ route('metode.create') }}" class="btn btn-primary btn-sm mb-3">Metode Baru</a>
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Type</th>
									<th>Nama Metode</th>
									<th>QR</th>
									<th>Bank</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
							@forelse($metode as $item)
								<tr>
									<td>{{ $item->type }}</td>
									<td>{{ $item->name }}</td>
									<td>{!! $item->gambar ? '<img src="'.asset('img/'.$item->gambar).'" width="40">' : '-' !!}</td>
									<td>{!! $item->bank_id ? $item->bank()->atas_nama.'<br>'.$item->bank()->norek : '-' !!}</td>
									<td>
										<a href="{{ route('metode.edit',$item->id) }}" class="btn btn-primary btn-sm">Edit</a>
										<a href="{{ route('metode.destroy',$item->id) }}" class="btn btn-danger btn-sm btn-hapus">Hapus</a>
										<form action="{{ route('metode.destroy',$item->id) }}" method="post">
										@csrf
										@method('DELETE')
										</form>
									</td>
								</tr>
							@empty
								<tr>
									<td colspan="5" class="text-center bg-info text-white">Empty (0) Data</td>
								<tr>
							@endforelse
							</tbody>
						</table>
					</div>
					{{ $metode->links() }}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection