@extends('adminToko.base')

@section('main')

<div class="container mt-3 mb-3">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Home</a></li>
	    <li class="breadcrumb-item"><a href="{{ route('metode.index') }}">Metode Pembayaran</a></li>
	    <li class="breadcrumb-item active" aria-current="page">Baru</li>
	  </ol>
	</nav>

	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					<form method="POST" action="{{ route('metode.update', $metode->id) }}" enctype="multipart/form-data">
						@csrf
						@method('PUT')
						<div class="form-row">
							<div class="form-group col-md-6">
								<label for="bank">Type</label>
								<select class="form-control" name="type" id="type" required>
									<option value="">Pilih</option>
									<option value="cod" @if($metode->type == 'cod') selected @endif>COD (Ditempat)</option>
									<option value="tf" @if($metode->type == 'tf') selected @endif>Transfer</option>
								</select>
							</div>
							<div class="form-group col-md-6">
								<label for="name">Nama Metode</label>
								<input type="text" name="name" id="name" class="form-control" value="{{ $metode->name }}" required>
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-6">
								<label for="qr">QR Code</label>
								<input type="file" name="qr" id="qr" class="form-control" accept="image/jpeg">
								@if($metode->gambar)
								<img src="{{ asset('img/'.$metode->gambar) }}" width="150" class="mt-3">
								@endif
							</div>
							<div class="form-group col-md-6">
								<label for="bank">Bank</label>
								<select class="form-control" name="bank" id="bank">
									<option value="">Pilih</option>
									@foreach($bank as $item)
									<option value="{{ $item->id_bank_toko }}" @if($metode->bank_id == $item->id_bank_toko) {{ 'selected' }} @endif>{{ $item->nama }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<button type="submit" class="btn btn-sm btn-success">Simpan</button>
					</form>
					<p class="mt-3">Jika type COD silahkan inputkan gambar, jika transfer maka hanya perlu memilih bank</p>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection