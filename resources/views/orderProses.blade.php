<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Orderan</title>
  </head>
  <body>
    <div class="container">
    @if(!session()->get('loginOrder'))
        <form method="POST">
        @csrf
            <div class="row justify-content-md-center">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" name="password" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Login</button>
                </div>
            </div>
        </form>
    @else
        @if(request()->get('id'))
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4>Detail Pesanan {{ $order->id_order }}</h4>
                        <a class="btn btn-primary mb-3" href="{{ route('orderproses') }}">Kembali</a>
                        <div>
                            Nomor Orderan : {{ $order->id_order }}<br>
                            Nama : <b>{{ $order->nama }}</b> <br>
                            Tanggal Orderan : {{ tgl_indo($order->created_at) }}<br>
                            Metode Pembayaran : {{ $metodeOrder ? $metodeOrder->name : $order->metode }}<br>
                            Status : <b>{{ ucfirst($order->status) }}</b> <br><br>
                            <div class="email"><a href="https://api.whatsapp.com/send?phone=62{{ $order->nohp }}&amp;text=Halo.." target="_blank"> <h6 style="color:#f6b20e; font-size:18px" class="text-center pt-3"><img src="http://pasarpedia.id/img/waicon.png"> Whatsapp </h6> </a> <br>
                            </div>

                            <span>Produk yang diorder</span>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Nama Produk</th>
                                        <th>Modal</th>
                                        <th>Harga</th>
                                        <th>Qty</th>
                                        <th>Jumlah</th>
                                    </tr>
                                </thead>
                                <tbody>
                            <?php $total = $diskon = $modal = 0; ?>
                            @foreach($order->detail($order->id_order) as $item)
                                <tr>
                                    <td>{{ $item->nm_produk}}</td>
                                    <td class="text-right">{{ rupiah($item->modal) }} /{{ $item->satuan }}</td>
                                    <td class="text-right">{{ rupiah($item->harga) }} /{{ $item->satuan }}</td>
                                    <td class="text-right">{{ $item->jumlah }}</td>
                                    <td class="text-right">{{ rupiah($item->jumlah*$item->harga) }}</td>
                                    <?php $total = $total+($item->jumlah*$item->harga); $diskon = $diskon + $item->diskon; $modal = $modal + ( $item->modal * $item->jumlah );?>
                                </tr>
                            @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="3">Total Modal</th>
                                        <th class="text-right">{{ rupiah($modal) }}</th>
                                    </tr>
                                    <tr>
                                        <th colspan="4">Total Belanja</th>
                                        <th class="text-right">{{ rupiah($total) }}</th>
                                    </tr>
                                    <tr>
                                        <th colspan="4">Diskon</th>
                                        <th class="text-right">@if($diskon > 0) - @endif{{ rupiah($diskon) }}</th>
                                    </tr>
                                    <tr>
                                        <th colspan="4">Jasa belanja & Antar</th>
                                        <th class="text-right">{{ rupiah($order->total - ($total)) }}</th>
                                    </tr>
                                    <tr>
                                        <th colspan="4">Total Tagihan</th>
                                        <th class="text-right">{{ rupiah($order->total) }}</th>
                                    </tr>
                                </tfoot>
                            </table>
                            @if($order->note)
                            <div class="mt-3" style="border-radius: 10px; border: 1px dashed #ffb300; padding: 10px;">
                                <h5>NOTE :</h5>
                                <p>{{ $order->note }}</p>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="card mt-3">
                    <div class="card-body">
                        <ul class="list-group mb-2 mt-2">
                            <li class="list-group-item rounded-0">
                                <h6 class="mb-4"><b>COD (bayar ditempat)</b></h6>
                                @foreach($metode->where('type','cod') as $k => $v)
                                <div class="form-group">
                                    <label style="width:100%;">{{ $v->name }}</label>
                                    <img style="max-width:100%;" src="{{ asset('img/'.$v->gambar) }}">
                                </div>
                                @endforeach
                            </li>
                            <li class="list-group-item rounded-0">
                                <h6 class="mb-4"><b>Bank Transfer</b><p>Transfer pada salah satu bank dibawah ini :</p></h6>
                                @foreach($metode->where('type','tf') as $k => $v)   
                                <div class="form-group">
                                    <span>{{ $v->name }}</span><br>
                                    <span>Rekening :<b> {{ $v->bank()->norek }} </b></span><br>
                                    <span>Atas nama : {{ $v->bank()->atas_nama }}</span>
                                </div>
                                @endforeach
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        @else
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4>Daftar Pesanan</h4>
                        <div class="table-responsive">
                            <table class="table table-bordered datatableO">
                                <thead>
                                    <tr>
                                        <th>No Orderan</th>
                                        <th>Status</th>
                                        <th>Tanggal</th>
                                        <th>Pemesan</th>
                                        <th>Alamat</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @forelse($order as $item)
                                    <tr>
                                        <td>{{ nopesan(Auth::guard('toko')->id(),$item->id_order,$item->created_at) }}</td>
                                        <td>{{ ucfirst($item->status) }}</td>
                                        <td>{{ $item->created_at }}</td>
                                        <td>{{ $item->nohp }}<br>{{ $item->nama }}</td>
                                        <td>{{ $item->alamat }}</td>
                                        <td>
                                            <a href="{{ route('orderproses', ['id'=> $item->id_order]) }}" class="btn btn-sm btn-primary">Detail</a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="6">Tidak ada data</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                            {{ $order->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    @endif
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>
</html>