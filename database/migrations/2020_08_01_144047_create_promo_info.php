<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromoInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promo_infos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type')->default('promo');
            $table->string('title');
            $table->text('gambar');
            $table->text('description');
            $table->boolean('hapus')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promo_infos');
    }
}
