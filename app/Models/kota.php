<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class kota extends Model
{
    protected $table = "otw_kota";
    protected $primaryKey = "id_kota";
}
