<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\bank;

class bankToko extends Model
{
    protected $table = "otw_bank_toko";
    protected $primaryKey = "id_bank_toko";

    protected $fillable = [
    	'id_bank',
    	'id_toko',
    	'atas_nama',
    	'norek'
    ];

    public function getBankAttribute(){
    	$bank = bank::find($this->id_bank);

    	return $bank;
    }
}
