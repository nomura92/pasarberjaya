<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\toko;

class keranjang extends Model
{
    protected $table = "otw_keranjang";
    protected $primaryKey = "id_keranjang";

    protected $fillable = [
    	'id_member',
        'id_toko'
    ];

    protected $attributes = [
        'status' => 0,
        'note' => null
    ];

    public function getPitih($harga){
        return "Rp ".number_format($harga,0,',','.');
    }

    public function getTotalAttribute(){
        $isiKeranjang = \DB::table('otw_keranjang_detail')->selectRaw('sum((otw_produk.harga-otw_produk.diskon)*otw_keranjang_detail.jumlah) as total' )->join('otw_produk', 'otw_produk.id_produk', '=', 'otw_keranjang_detail.id_produk')->where('otw_keranjang_detail.id_keranjang', $this->id_keranjang)->first();

        return $isiKeranjang ? $isiKeranjang->total : 0;
    }

    public function getTokoAttribute(){

        return toko::find($this->id_toko);
    }

}
