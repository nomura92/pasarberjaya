<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class referral extends Authenticatable
{
    protected $table = "otw_ref";
    protected $primaryKey = "id_ref";

    protected $fillable = [
    	'kode',
    	'nama',
    	'password',
    	'id_toko'
    ];

    protected $hidden = [
        'password', 'remember_token'
    ];

    protected $attributes = [
        'remember_token' => ''
    ];
}
