<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class provinsi extends Model
{
    protected $table = "otw_provinsi";
    protected $primaryKey = "id_provinsi";
}
