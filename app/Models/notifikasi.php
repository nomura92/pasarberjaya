<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class notifikasi extends Model
{
    protected $table = "otw_notifikasi";
    protected $primaryKey = "id_notifikasi";

    protected $fillable = [
    	'id_member',
        'id_toko',
        'judul',
        'isi'
    ];

    protected $attributes = [
        'dilihat' => 0
    ];
}
