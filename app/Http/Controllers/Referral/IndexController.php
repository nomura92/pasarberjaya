<?php

namespace App\Http\Controllers\Referral;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\Models\toko;
use App\Models\member;

class IndexController extends Controller
{
    public function index(){

    	$toko = toko::find(Auth::guard('ref')->user()->id_toko);
    	$member = member::where('ref',Auth::guard('ref')->user()->kode)->get();
    	$order = \DB::table('otw_order')->join('otw_member','otw_order.id_member','=','otw_member.id_member')->select(\DB::raw('sum(otw_order.total) as total'))->where('otw_order.id_toko', Auth::guard('ref')->user()->id_toko)->where('otw_order.bayar',1)->where('otw_member.ref', Auth::guard('ref')->user()->kode)->first();
    	return view('referral.dashboard', compact('toko', 'member', 'order'));
    }
}
