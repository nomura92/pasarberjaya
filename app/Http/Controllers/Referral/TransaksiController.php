<?php

namespace App\Http\Controllers\Referral;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\member;
use App\Models\order;
use Auth;

class TransaksiController extends Controller
{
    public function index(Request $request){
    	$bulan = date('m');
    	$tahun = date('Y');
    	if($request->get('bulan')){
    		$bulan = $request->get('bulan');
    	}
    	if($request->get('tahun')){
    		$tahun = $request->get('tahun');
    	}

    	$transaksi = \DB::table('otw_order')->join('otw_member','otw_order.id_member','=','otw_member.id_member')->select(\DB::raw('date(otw_order.created_at) date,count(otw_order.id_order) as jumlah, sum(otw_order.total) as total'))->where('otw_order.id_toko', Auth::guard('ref')->user()->id_toko)->where('otw_order.bayar',1)->where('otw_member.ref', Auth::guard('ref')->user()->kode)->whereMonth('otw_order.created_at',$bulan)->whereYear('otw_order.created_at',$tahun)->groupBy('date')->get();
    	return view('referral.transaksi.index', compact('bulan','tahun','transaksi'));
    }

    public function perhari($tanggal){
    	$transaksi = \DB::table('otw_order')->join('otw_member','otw_order.id_member','=','otw_member.id_member')->select(\DB::raw('otw_member.nama,date(otw_order.created_at) date,count(otw_order.id_order) as jumlah, sum(otw_order.total) as total'))->where('otw_order.id_toko', Auth::guard('ref')->user()->id_toko)->where('otw_order.bayar',1)->where('otw_member.ref', Auth::guard('ref')->user()->kode)->where(\DB::raw('date(otw_order.created_at)'), $tanggal)->get();
    	return view('referral.transaksi.hari', compact('tanggal','transaksi'));
    }
}
