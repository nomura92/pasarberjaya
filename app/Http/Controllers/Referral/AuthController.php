<?php

namespace App\Http\Controllers\Referral;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;

class AuthController extends Controller
{
    
    public function login(){
    	return view('referral.auth.login');
    }

    public function loginact(Request $request){
    	$username = $request->get('username');
    	$password = $request->get('password');

    	if (Auth::guard('ref')->attempt(['kode' => $username, 'password' => $password ],true)) {
            return redirect()->route('ref.index');
		}else{
            return redirect()->route('ref.login');
        }
    }

    public function logout(){
        Auth::guard('ref')->logout();
        return redirect()->route('ref.login');
    }

}
