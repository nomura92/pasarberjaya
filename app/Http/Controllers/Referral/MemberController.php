<?php

namespace App\Http\Controllers\Referral;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\member;
use App\Models\order;
use Auth;

class MemberController extends Controller
{
    public function index(){

    	$member = member::where('ref', Auth::guard('ref')->user()->kode)->paginate(10);
    	$order = \DB::table('otw_order')->select(\DB::raw('otw_order.total, otw_order.id_member'))->where('otw_order.id_toko', Auth::guard('ref')->user()->id_toko)->where('bayar', 1)->get();
    	return view('referral.member.index', compact('member', 'order'));
    }
}
