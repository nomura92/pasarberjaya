<?php

namespace App\Http\Controllers\Referral;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\Models\bank;
use App\Models\referral;

class ProfilController extends Controller
{
    public function index(){
    	$profil = Auth::guard('ref')->user();
    	$bank = bank::all();
    	return view('referral.profil.index', compact('profil', 'bank'));
    }

    public function bank(Request $request){
    	$request->validate([
            'bank'=>'required',
            'nama'=>'required',
            'norek'=>'required',
        ]);

        $referral = referral::find(Auth::guard('ref')->user()->id_ref);
    	$referral->id_bank = $request->get('bank');
    	$referral->atasnama = $request->get('nama');
    	$referral->norek = $request->get('norek');
    	$referral->save();

    	return redirect()->route('ref.profil');
    }

    public function pass(Request $request){
    	$request->validate([
            'password'=>'required|confirmed',
        ]);

        $referral = referral::find(Auth::guard('ref')->user()->id_ref);
        $referral->password = \Hash::make($request->get('password'));
        $referral->save();

        return redirect()->route('ref.profil');
    }
}
