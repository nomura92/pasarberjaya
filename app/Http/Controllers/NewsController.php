<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\notifikasi;
use App\Models\toko;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($uid)
    {
        //
        $toko = toko::where('seo_toko',$uid)->first();
        $title = "Notifikasi";
        
        return view('frontend.news.index',compact('title','toko'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($uid, $id)
    {
        //
        $toko = toko::where('seo_toko',$uid)->first();
        $title = "Tampil Semua Pengumuman";

        $newsToko = notifikasi::find($id);

        $newsToko->dilihat = 1;
        $newsToko->save();
        
        return view('frontend.news.show',compact('title','toko','newsToko'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
