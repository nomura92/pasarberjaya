<?php

namespace App\Http\Controllers\Auth;

use App\Models\member;
use App\Models\toko;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Validation\Rule;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm($uid = null)
    {
        if($uid)
            $toko = toko::where('seo_toko',$uid)->first();
        else
            $toko = toko::find(10);

        $title = "Daftar member baru";
        return view('apk.customauth.register',compact('title','toko'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        return Validator::make($data, [
            'nohp' => ['required', 'string', 'max:255', 'unique:otw_member'],
            'password' => ['required', 'string', 'min:8']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        
        return member::create([
            'nohp' => $data['nohp'],
            'password' => Hash::make($data['password']),
            'password_text' => $data['password'],
            'ref' => $data['ref']
        ]);
    }

    public function redirectTo(){
        return back();
    }
}
