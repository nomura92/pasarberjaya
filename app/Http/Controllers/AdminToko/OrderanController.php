<?php

namespace App\Http\Controllers\AdminToko;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\order;
use App\Models\orderDetail;
use App\Models\produk;
use Auth;
use App\Models\method;

class OrderanController extends Controller
{
    //
    private $folder = "order/";

    public function index(){
    	$title = "Modul Orderan :: Administrator";
        $toko = Auth::guard('toko')->id();

    	$order = order::where('id_toko', $toko)->get();
        $datatable = true;
        
    	return view('adminToko/'.$this->folder.__FUNCTION__,compact('title','order','datatable'));
    }

    public function status($status){
    	$title = "Modul Orderan ".ucfirst($status)." :: Administrator";
        $toko = Auth::guard('toko')->id();

    	$order = order::where('id_toko', $toko)->where('status',$status)->get();
    	$stat = array('pending','proses','sukses','batal');
        $datatable = true;

    	return view('adminToko/'.$this->folder.__FUNCTION__,compact('title','order','status','stat','datatable'));
    }

    public function show($id){
    	$title = "Orderan no $id :: Administrator";
        $toko = Auth::guard('toko')->id();

    	$order = order::where('id_toko', $toko)->where('id_order',$id)->first();
        $metode = explode('-', $order->metode);
        if(count($metode) > 1)
            $metode = method::find($metode[1]);
        else
            $metode = null;
    	return view('adminToko/'.$this->folder.__FUNCTION__,compact('title','order','metode'));
    }

    public function update(Request $request, $id){
    	$request->validate([
            'status'=>'required'
        ]);

        $toko = Auth::guard('toko')->id();

        $order = order::where('id_toko', $toko)->where('id_order',$id)->first();
        $order->status = $request->get('status');
        if($request->get('status')=="sukses"){
            $order->bayar = 1;
            foreach($order->detail($id) as $item){
                $produk = produk::find($item->id_produk);
                $produk->stok = $produk->stok - $item->jumlah;
                $produk->terjual = $produk->terjual + $item->jumlah;
                $produk->save();
            }
        }
        $order->save();
        
        return redirect('tokoku/orderan')->with('success','Berhasil diganti');
    }

    public function getDelete($id){

        $detail = orderDetail::findOrFail($id);
        $order = order::findOrFail($detail->id_order);
        //recalculate
        $total_delete = $detail->jumlah * $detail->harga;
        $order->total = $order->total - $total_delete;
        $order->save();
        //update stok produk
        $produk = produk::findOrFail($detail->id_produk);
        $produk->stok = $produk->stok + $detail->jumlah;
        $produk->save();

        $detail->delete();

        return redirect()->back()->with('success','Item berhasil dihapus');
    }

    public function cetak($id){
        $toko = Auth::guard('toko')->id();

        $order = order::where('id_toko', $toko)->where('id_order',$id)->first();

        $title = "Cetak Orderan ".nopesan($toko, $order->id_order, $order->created_at);
        return view('adminToko/'.$this->folder.__FUNCTION__,compact('title','order'));
    }

    public function cetakbesar($id){
        $toko = Auth::guard('toko')->id();

        $order = order::where('id_toko', $toko)->where('id_order',$id)->first();

        $title = "Cetak Orderan ".nopesan($toko, $order->id_order, $order->created_at);
        return view('adminToko/'.$this->folder.__FUNCTION__,compact('title','order'));
    }

    public function create(){
        $toko = Auth::guard('toko')->id();
        $title = "Add Item ";
        $order = order::findOrFail(request()->get('id'));
        $produk = produk::select('id_produk','nm_produk','harga','diskon', 'satuan')->whereIdToko($toko)->whereHapus(0)->get();
        return view('adminToko.order.create', compact('title', 'order', 'produk'));
    }

    public function store(Request $req){
        
        $order = order::findOrFail($req->order);
        //all new item
        $total = 0;
        foreach($req->produk as $k => $v){
            //produk
            $produk = produk::findOrFail($v);
            //add detail
            $detail = new orderDetail;
            $detail->id_order = $order->id_order;
            $detail->id_produk = $v;
            $detail->jumlah = $req->jumlah[$k];
            $detail->harga = $produk->harga;
            $detail->diskon = $produk->diskon;
            $detail->modal = $produk->modal;
            $detail->save();

            //kurangi stok
            $produk->stok = $produk->stok - $req->jumlah[$k];
            $produk->save();

            $total = $total + (($produk->harga - $produk->diskon) * $req->jumlah[$k]);
        }
        $order->total = $order->total + $total;
        $order->save();

        return redirect()->route('orderan.show', $req->order)->with('success','Item berhasil ditambahkan');
    }
}
