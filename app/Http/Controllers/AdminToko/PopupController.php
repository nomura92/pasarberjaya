<?php

namespace App\Http\Controllers\AdminToko;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\toko;
use App\Models\popup;
use Auth;
use Image;

class PopupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Popup :: Administrator Toko";
        $popup = popup::whereTokoId(Auth::guard('toko')->id())->where('hapus',0)->paginate(15);
        return view('adminToko.popup.index', compact('title','popup'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "Popup :: Administrator Toko";
        return view('adminToko.popup.create', compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title'=>'required',
        ]);

        $popup = new popup;
        $popup->toko_id = Auth::guard('toko')->id();
        $popup->header = $request->title;
        $popup->description = $request->description;
        if($request->file('image')){
            $uploadedFile = $request->file('image');
            $thumbnailImage = Image::make($uploadedFile);
            $thumbnailPath = 'img/popup/';
            if(!file_exists($thumbnailPath)){
                mkdir($thumbnailPath, 0755, true);
            }
            $thumbnailImage->resize(600, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            $filename = str_replace(' ','',time().$uploadedFile->getClientOriginalName());
            $thumbnailImage->save($thumbnailPath.$filename);

            $popup->image = $filename;
        }
        $popup->save();

        return redirect()->route('popup.index')->with('success','Berhasil menambahkan popup baru');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(request()->get('tampil') >= 0){
            \DB::table('popups')->update(['tampil' => 0]);
            $popup = popup::findOrFail($id);
            $popup->tampil = request()->get('tampil');
            $popup->save();
            return redirect()->route('popup.index')->with('success','Berhasil menambahkan popup baru');
        }else return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = "Edit Popup :: Administrator Toko";
        $popup = popup::findOrFail($id);

        return view('adminToko.popup.edit',compact('title','popup'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title'=>'required',
        ]);

        $popup = popup::findOrFail($id);
        $popup->header = $request->title;
        $popup->description = $request->description;
        if($request->file('image')){
            $uploadedFile = $request->file('image');
            $thumbnailImage = Image::make($uploadedFile);
            $thumbnailPath = 'img/popup/';
            if(!file_exists($thumbnailPath)){
                mkdir($thumbnailPath, 0755, true);
            }
            $thumbnailImage->resize(600, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            $filename = str_replace(' ','',time().$uploadedFile->getClientOriginalName());
            $thumbnailImage->save($thumbnailPath.$filename);

            $popup->image = $filename;
        }
        $popup->save();

        return redirect()->route('popup.index')->with('success','Berhasil mengubah data popup');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $popup = popup::find($id);
        $popup->delete();

        return redirect()->route('popup.index')->with('success','Berhasil menghapus data popup');
    }
}
