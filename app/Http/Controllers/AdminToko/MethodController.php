<?php

namespace App\Http\Controllers\AdminToko;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\bankToko;
use App\Models\method;
use Auth;
use Image;

class MethodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Metode Pembayaran :: Administrator Toko";
        $metode = method::whereIdToko(Auth::guard('toko')->id())->where('tampil',1)->where('hapus',0)->paginate(15);
        return view('adminToko.metodebayar.index', compact('title','metode'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "Metode Pembayaran :: Administrator Toko";
        $method = method::whereIdToko(Auth::guard('toko')->id())->whereTampil(1)->whereHapus(0)->get();
        $bank = bankToko::where('otw_bank_toko.id_toko',Auth::guard('toko')->id())->where('otw_bank_toko.hapus',0)->join('otw_bank', 'otw_bank.id_bank','=','otw_bank_toko.id_bank')->paginate(15);
        return view('adminToko.metodebayar.create', compact('title','method','bank'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$metode = new method;
        $metode->id_toko = Auth::guard('toko')->id();
    	switch($request->type){
    		case 'cod':
    			$request->validate([
		            'name'=>'required',
		            'qr'=>'required'
		        ]);
		        $uploadedFile = $request->file('qr');

		        $thumbnailImage = Image::make($uploadedFile);
		        $thumbnailPath = 'img/';
		        $thumbnailImage->resize(600, null, function ($constraint) {
		            $constraint->aspectRatio();
		            $constraint->upsize();
		        });
		        $filename = str_replace(' ','',time().$uploadedFile->getClientOriginalName());
		        $thumbnailImage->save($thumbnailPath.$filename);

		        $metode->gambar = $filename;
    		break;
    		case 'tf':
    			$request->validate([
		            'bank'=>'required',
		            'name'=>'required',
		        ]);
		        $metode->bank_id = $request->bank;
    		break;
    	}
    	$metode->type = $request->type;
    	$metode->name = $request->name;
        $metode->save();

        return redirect()->route('metode.index')->with('success','Berhasil menambahkan metode pembayaran baru');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = "Edit Metode Pembayaran :: Administrator Toko";
        $metode = method::findOrFail($id);
        $bank = bankToko::where('otw_bank_toko.id_toko',Auth::guard('toko')->id())->where('otw_bank_toko.hapus',0)->join('otw_bank', 'otw_bank.id_bank','=','otw_bank_toko.id_bank')->paginate(15);

        return view('adminToko.metodebayar.edit',compact('title','bank', 'metode'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$metode = method::findOrFail($id);
        switch($request->type){
    		case 'cod':
    			$request->validate([
		            'name'=>'required',
		            'qr'=>'required'
		        ]);
		        $uploadedFile = $request->file('qr');

		        $thumbnailImage = Image::make($uploadedFile);
		        $thumbnailPath = 'img/';
		        $thumbnailImage->resize(600, null, function ($constraint) {
		            $constraint->aspectRatio();
		            $constraint->upsize();
		        });
		        $filename = str_replace(' ','',time().$uploadedFile->getClientOriginalName());
		        $thumbnailImage->save($thumbnailPath.$filename);

		        $metode->gambar = $filename;
    		break;
    		case 'tf':
    			$request->validate([
		            'bank'=>'required',
		            'name'=>'required',
		        ]);
		        $metode->bank_id = $request->bank;
    		break;
    	}

        $metode->type = $request->type;
    	$metode->name = $request->name;
        $metode->save();

        return redirect()->route('metode.index')->with('success','Berhasil mengubah data metode');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $metode = method::findOrFail($id);
        $metode->hapus = 1;
        $metode->save();

        return redirect()->route('metode.index')->with('success','Berhasil menghapus data metode');
    }
}
