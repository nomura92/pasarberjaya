<?php

namespace App\Http\Controllers\AdminToko;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\Models\referral;
use App\Models\member;
use App\Models\order;
use App\Models\bank;

class ReferralController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Modul Referral :: Administrator";
        $toko = Auth::guard('toko')->id();

        $referral = referral::where('id_toko',$toko)->get();
        $bank = bank::all();
        $datatable = true;
        return view('adminToko.referral.index',compact('title','referral','datatable','bank'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "Modul Referral :: Administrator";

        return view('adminToko.referral.create',compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kode'=>'required|unique:otw_ref',
            'nama'=>'required',
            'password'=>'required',
        ]);

        $toko = Auth::guard('toko')->user()->id_toko;

        $referral = new referral([
            'id_toko' => $toko,
            'kode' => $request->get('kode'),
            'nama' => $request->get('nama'),
            'password' => \Hash::make($request->get('password'))
        ]);
        $referral->save();

        return redirect()->route('referral.index')->with('success','Berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $title = "Modul Referral :: Administrator";
        $ref = referral::find($id);

        $member = member::where('ref', $ref->kode)->get();
        $order = order::where('id_toko', Auth::guard('toko')->id());

        $bulan = date('m');
        $tahun = date('Y');
        if($request->get('bulan')){
            $bulan = $request->get('bulan');
        }
        if($request->get('tahun')){
            $tahun = $request->get('tahun');
        }

        $transaksi = \DB::table('otw_order')->join('otw_member','otw_order.id_member','=','otw_member.id_member')->select(\DB::raw('date(otw_order.created_at) date,count(otw_order.id_order) as jumlah, sum(otw_order.total) as total'))->where('otw_order.id_toko', $ref->id_toko)->where('otw_order.bayar',1)->where('otw_member.ref', $ref->kode)->whereMonth('otw_order.created_at',$bulan)->whereYear('otw_order.created_at',$tahun)->groupBy('date')->get();
        return view('adminToko.referral.show',compact('title','order','member','transaksi','bulan','tahun'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = "Modul Referral :: Administrator";
        $referral = referral::find($id);
        return view('adminToko.referral.edit',compact('title', 'referral'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kode'=>'required',
            'nama'=>'required',
        ]);

        $referral = referral::find($id);
        $referral->kode = $request->get('kode');
        $referral->nama = $request->get('nama');
        if($request->get('password')){
            $referral->password = \Hash::make($request->get('password'));
        }
        $referral->save();

        return redirect()->route('referral.index')->with('success','Berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
