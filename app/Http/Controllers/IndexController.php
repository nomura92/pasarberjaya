<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\kategoriProduk;
use App\Models\produk;
use App\Models\toko;
use App\Models\slide;
use App\Models\order;
use App\Models\method;
use App\Models\popup;

class IndexController extends Controller
{
    public function index($uid){
    	$toko = toko::where('seo_toko',$uid)->first();
        $title = $toko->nm_toko;

        $ktg_produk = kategoriProduk::where('id_toko',$toko->id_toko)->where('hapus',0)->get();
        $produk = produk::where('id_toko',$toko->id_toko)->where('hapus',0)->get();

        $slide = slide::where('id_toko',$toko->id_toko)->where('hapus',0)->get();
        $popup = popup::whereTokoId($toko->id_toko)->whereTampil(1)->first();

        return view('frontend.index', compact('title','toko','ktg_produk','produk','slide', 'popup'));
    }

    public function cari($uid,Request $request){
    	$toko = toko::where('seo_toko',$uid)->first();
        $title = 'Hasil pencarian kata kunci "'.$request->cari.'"';

        $produk = produk::where('id_toko',$toko->id_toko)->where('hapus',0)->where('nm_produk','LIKE','%'.$request->cari.'%')->get();
        if($request->cari==""){
        	$produk = produk::where('id_toko',$toko->id_toko)->where('hapus',0)->where('nm_produk','=',$request->cari)->get();
        }

        return view('frontend.cari', compact('title','toko','produk'));
    }

    public function orderProses($uid){
        $toko = toko::where('seo_toko',$uid)->first();
        $metode = method::whereIdToko($toko->id_toko)->whereTampil(1)->whereHapus(0)->get();
        $metodeOrder = null;

        if(session()->get('loginOrder') == 'testsaja')
            if(request()->get('id')){
                $order = order::findOrFail(request()->get('id'));
                $metodeOrder = explode('-', $order->metode);
                if(count($metodeOrder) > 1)
                    $metodeOrder = method::find($metodeOrder[1]);
            }
            else
                $order = order::whereIdToko($toko->id_toko)->whereStatus('proses')->paginate(20);
        else
            $order = collect();

        return view('frontend.orderProses', compact('order', 'metode','metodeOrder'));
    }

    public function postOrderProses($uid, Request $request){
        if($request->password == 'testsaja'){
            session(['loginOrder' => 'testsaja']);
        }

        return back();
    }
}
