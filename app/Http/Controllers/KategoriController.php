<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\produk;
use App\Models\kategoriProduk;
use App\Models\toko;

class KategoriController extends Controller
{
    //
    public function show($uid, $id , $slug){
    	$toko = toko::where('seo_toko',$uid)->first();

        $kategori = kategoriProduk::where('id_toko',$toko->id_toko)->where('id_ktg_produk', $id)->first();
        $title = $kategori->nm_ktg_produk;
        $produk = produk::where('id_toko',$toko->id_toko)->where('hapus', 0)->where('id_ktg_produk', $id)->paginate(20);

        return view('frontend.kategori.detail', compact('title','produk','kategori','toko'));
    }

    public function promo($uid){
    	$toko = toko::where('seo_toko',$uid)->first();
    	$title = "Promo";
        $produk = produk::where('id_toko',$toko->id_toko)->where('hapus', 0)->where('promo', 1)->paginate(20);

        return view('frontend.kategori.pdtt', compact('title','produk','toko'));
    }

    public function diskon($uid){
    	$toko = toko::where('seo_toko',$uid)->first();
        $title = "Diskon";
        $produk = produk::where('id_toko',$toko->id_toko)->where('hapus', 0)->where('diskon','>', 0)->orderBy('diskon','desc')->paginate(20);

        return view('frontend.kategori.pdtt', compact('title','produk','toko'));
    }

    public function terbaru($uid){
        $toko = toko::where('seo_toko',$uid)->first();
        $title = "Terbaru";
        $produk = produk::where('id_toko',$toko->id_toko)->where('hapus', 0)->orderBy('created_at','desc')->paginate(20);

        return view('frontend.kategori.pdtt', compact('title','produk','toko'));
    }

    public function terlaris($uid){
        $toko = toko::where('seo_toko',$uid)->first();
        $title = "Terlaris";
        $produk = produk::where('id_toko',$toko->id_toko)->where('hapus', 0)->where('terjual','>',0)->orderBy('terjual','desc')->paginate(20);

        return view('frontend.kategori.pdtt', compact('title','produk','toko'));
    }
}
