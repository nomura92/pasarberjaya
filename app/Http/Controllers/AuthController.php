<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\member;
use App\Models\toko;

class AuthController extends Controller
{
    public function getLogin(){
    	if(Auth::check())
    		return redirect()->route('apk.index');
    	$title = "Login User";
        $toko = toko::find(10);
        return view('apk.customauth.login',compact('title', 'toko'));
    }

    public function postLogin(Request $request){
    	$request->validate([
    		'nohp' => 'required',
    		'password' => 'required'
    	]);

    	if (Auth::attempt(['nohp' => $request->get('nohp'), 'password' => $request->get('password')], $request->get('remember_me'))) {
		    return redirect()->back();
		}

		return redirect()->back()->with('error', 'Username tidak ditemukan.');

    }

    public function getRegister(){
    	if(Auth::check())
    		return redirect()->route('apk.index');
    	$title = "Register User";
        $toko = toko::find(10);
        return view('apk.customauth.register',compact('title', 'toko'));
    }

    public function postRegister(Request $request){
    	$request->validate([
    		'nohp' => ['required', 'string', 'max:255', 'unique:otw_member'],
            'password' => ['required', 'string', 'min:8']
    	]);

    	$member = new member;
        $member->nohp = $request->get('nohp');
        $member->password = \Hash::make($request->get('password'));
        $member->password_text = $request->get('password');
        $member->ref = $request->get('ref');
        $member->save();

        Auth::loginUsingId($member->id_member);

    	return redirect()->back();
    }

    public function getLogout(){
    	Auth::guard('web')->logout();

        return redirect()->route('apk.index');
    }
}
