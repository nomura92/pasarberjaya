<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\slide;
use Image;
use Storage;
use Str;
use Auth;
use Helper;

class SlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Modul Slide :: Administrator";

        $slide = slide::where('id_toko',0)->where('hapus',0)->paginate(15);
        return view('admin.slide.index',compact('title','slide'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "Modul Slide Baru :: Administrator";
        return view('admin.slide.create',compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'gambar'=>'required|image',
        ]);

        $filename = Helper::UploadImg($request->file('gambar'), 'uploads/slides/');

        $slide = new slide;
        $slide->gambar = $filename;
        $slide->id_toko = 0;
        $slide->save();

        return redirect()->route('slides.index')->with('success','Berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slide = slide::where('id_slide',$id)->where('id_toko',0)->firstOrFail();
        $slide->hapus = 1;
        $slide->save();

        return redirect()->route('slides.index')->with('success','Berhasil dihapus');
    }
}
