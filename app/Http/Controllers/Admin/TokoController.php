<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\toko;
use App\Models\produk;
use App\Models\order;
use App\Models\provinsi;
use App\Models\kota;
use Hash;
use Str;

class TokoController extends Controller
{
    private $folder = "toko/";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Modul Toko :: Administrator";
        // $toko = \DB::table('otw_toko')->join('otw_kota','otw_toko.id_kota','=','otw_kota.id_kota')->join('otw_provinsi','otw_provinsi.id_provinsi','=','otw_kota.id_provinsi')->where('hapus', 0)->get();
        $toko = toko::where('hapus', 0)->get();
        $kota = kota::all();

        return view('admin/'.$this->folder.__FUNCTION__,compact('title','toko', 'kota'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $title = "Modul Toko Baru :: Administrator";
        $provinsi = provinsi::all()->toJson();
        $kota = kota::all()->toJson();

        return view('admin/'.$this->folder.__FUNCTION__,compact('title', 'provinsi', 'kota'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $messages = [ 
            'same'    => ':attribute dan :other harus sama.',
            'size'    => ':attribute memiliki panjang :size karakter.',
            'username.unique' => 'Username sudah digunakan oleh toko lain.',
            'password.confirmed' => 'Password dan konfirmasi password berbeda.'
        ];
        $request->validate([
            'email'=>'required',
            'namap'=>'required',
            'kontakp'=>'required',
            'kontak'=>'required',
            'alamat'=>'required',
            'namat' => 'required',
            'seo_toko' => 'required|unique:otw_toko',
            'username'=>'required|min:4|unique:otw_toko',
            'password'=>'required|min:8|confirmed',
            'kota'=>'required'
        ],$messages);

        $toko = new toko;
        $toko->email= $request->get('email');
        $toko->nm_pengelola = $request->get('namap');
        $toko->no_telp_pengelola = $request->get('kontakp');
        $toko->nm_toko = $request->get('namat');
        $toko->no_telp_toko = $request->get('kontak');
        $toko->alamat_toko = $request->get('alamat');
        $toko->username = $request->get('username');
        $toko->password = Hash::make($request->get('password'));
        $toko->password_text = $request->get('password');
        $toko->seo_toko = Str::slug($request->get('seo_toko'));
        $toko->id_kota = $request->get('kota');
        $toko->save();

        if($request->get('aktifkan')){
            $toko->status = 1;
            $toko->aktif = date('Y-m-d G:i:s');
            $toko->save();
        }

        return redirect('admin/toko')->with('success','Berhasil menamahkan toko baru');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $title = "Modul Detail Toko :: Administrator";

        $toko = toko::find($id);
        $produk = produk::where('id_toko',$id)->paginate(8);
        $order = order::where('id_toko',$id)->get();
        return view('admin/'.$this->folder.__FUNCTION__,compact('title','toko','produk','order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $title = "Modul Edit Toko :: Administrator";
        $provinsi = provinsi::all();
        $kota = kota::all();

        $toko = toko::find($id);
        $toko['id_provinsi'] = $kota->where('id_kota', $toko->id_kota)->first()['id_provinsi'];
        return view('admin/'.$this->folder.__FUNCTION__,compact('title','toko', 'provinsi', 'kota'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'email'=>'required',
            'namap'=>'required',
            'kontakp'=>'required',
            'kontak'=>'required',
            'kota'=>'required'
        ]);

        $toko = toko::find($id);
        $toko->email = $request->get('email');
        $toko->nm_pengelola = $request->get('namap');
        $toko->no_telp_pengelola = $request->get('kontakp');
        $toko->no_telp_toko = $request->get('kontak');
        $toko->id_kota = $request->get('kota');
        $toko->featured = $request->get('featured');
        $toko->save();

        return redirect('admin/toko')->with('success','Berhasil mengubah data toko');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $toko = toko::find($id);
        $toko->hapus = 1;
        $toko->save();

        return redirect('admin/toko')->with('success','Berhasil menghapus toko.');
    }

    public function perpanjang(){
        $title = "Modul Perpanjang Toko :: Administrator";

        $toko = toko::where('perpanjang',1)->paginate(15);
        return view('admin/'.$this->folder.__FUNCTION__,compact('title','toko'));
    }

    public function aktifkan($id)
    {
        //
        $toko = toko::find($id);
        $toko->perpanjang = 0;
        $toko->tgl_pengajuan = "0000-00-00 00:00:00";
        if($toko->status > 0){
            $toko->aktif = date('Y-m-d G:i:s', strtotime('+1 years',strtotime($toko->aktif)));
        }else{
            $toko->status = 1;
            $toko->aktif = date('Y-m-d G:i:s');
        }
        $toko->save();

        return redirect('admin/toko/'.$id)->with('success','Berhasil mengaktifkan toko.');
    }

    public function batalkan($id)
    {
        //
        $toko = toko::find($id);
        $toko->perpanjang = 0;
        $toko->tgl_pengajuan = "0000-00-00 00:00:00";
        $toko->save();

        return redirect('admin/toko/perpanjang')->with('success','Berhasil membatalkan pengajuan.');
    }

    public function getFeatured($id){
        $toko = toko::findOrFail($id);
        $featured = request()->get('featured') ?: 0;
        if($featured > 1)
            return abort(403);
        $toko->featured = $featured;
        $toko->save();

        return redirect()->back()->with('success','Berhasil mengganti status featured.');       
    }
}
