<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Helper;
use App\Models\promoInfo;

class InformationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Modul Information :: Administrator";

        $data = promoInfo::whereType('information')->whereHapus(0)->paginate(15);
        return view('admin.promoInfo.index',compact('title','data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "Modul Information :: Administrator";

        return view('admin.promoInfo.create',compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'title' => 'required',
            'gambar'=>'required|image',
            'description' => 'required'
        ]);

        $data = new promoInfo;
        $data->type = 'information';
        $data->title = $request->get('title');
        $data->gambar = Helper::UploadImg($request->file('gambar'),'uploads/information/');
        $data->description = $request->get('description');
        $data->save();

        return redirect()->route('information.index')->with('success', 'Berhasil menambahkan information baru.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = "Modul Information :: Administrator";
        $data = promoInfo::whereType('information')->findOrFail($id);
        return view('admin.promoInfo.edit',compact('title', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'gambar'=>'nullable|image',
            'description' => 'required'
        ]);

        $data = promoInfo::findOrFail($id);
        $data->title = $request->get('title');
        if($request->has('gambar')){
            @unlink($data->gambar);
            $data->gambar = Helper::UploadImg($request->file('gambar'),'uploads/information/');
        }
        $data->description = $request->get('description');
        $data->save();

        return redirect()->back()->with('success', 'Berhasil mengubah data information.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $data = promoInfo::findOrFail($id);
        @unlink($data->gambar);
        $data->delete();

        return redirect()->back()->with('success', 'Berhasil menghapus data information.');
    }
}
