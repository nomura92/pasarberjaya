<?php

namespace App\Http\Controllers\Apk;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\toko;
use App\Models\slide;
use App\Models\provinsi;
use App\Models\kota;
use App\Models\member;
use App\Models\promoInfo;
use App\Models\keranjang;
use App\Models\order;
use App\Models\method;
use App\Models\produk;
use Auth;

class IndexController extends Controller
{
    public function index(Request $request){
    	$toko = toko::find(10);
    	if($request->get('kota')){
    		$allToko = toko::where('blokir', 0)->where('id_kota', $request->get('kota'))->whereHapus(0)->get();
    	}else{
    		$allToko = toko::whereBlokir(0)->whereFeatured(1)->whereHapus(0)->get();
    	}
    	$slides = slide::where('id_toko',0)->where('hapus',0)->get();

    	// $provinsi = provinsi::all()->toJson();
    	// $kota = kota::all()->toJson();

    	$Dkota = \DB::table('otw_provinsi')->join('otw_kota', 'otw_provinsi.id_provinsi', '=', 'otw_kota.id_provinsi')->get();
    	$kota = collect();
    	foreach($Dkota as $value){
    		$aKota = array('id'=> $value->id_kota, 'provinsi' => $value->nm_provinsi, 'kota' => $value->nm_kota);
    		$kota->push($aKota);
    	}
    	$kota->toJson();

        $promo = promoInfo::whereType('promo')->orderBy('created_at', 'desc')->get();
        $information = promoInfo::whereType('information')->orderBy('created_at', 'desc')->get();

    	return view('apk.index', compact('toko', 'allToko', 'slides', 'kota', 'promo', 'information'));
    }

    public function getMember(){
        $toko = toko::find(10);
        return view('apk.member', compact('toko'));
    }

    public function getProfil(){
        $toko = toko::find(10);
        return view('apk.profil', compact('toko'));
    }

    public function postProfil(Request $request){
        $request->validate([
            'nama'=>'required|string'
        ]);

        $member = member::findOrFail(Auth::user()->id_member);
        $member->nama = $request->get('nama');
        $member->alamat = $request->get('alamat');
        $member->save();

        return redirect()->back()->with('success','Berhasil update profil');
    }

    public function postPassword(Request $request){
        $request->validate([
            'password'=>'required|string|confirmed',
        ]);

        $member = member::findOrFail(Auth::user()->id_member);
        $member->password = Hash::make($request->get('password'));
        $member->save();

        return redirect()->back()->with('success','Berhasil update password');
    }

    public function getDetail($id){
        $data = promoInfo::findOrFail($id);
        $toko = toko::find(10);
        $title = $data->title;
        return view('apk.detail', compact('toko', 'data', 'title'));
    }

    public function getKeranjang(){
        $title = "Tampil Semua Keranjang";
        $id_member = Auth::user()->id_member;
        $toko = toko::find(10);

        $keranjang = keranjang::where('id_member',$id_member)->get();
        
        return view('apk.keranjang',compact('title','keranjang', 'toko'));
    }

    public function getOrder(){
        $title = "Tampil Semua Order";
        $id_member = Auth::user()->id_member;
        $toko = toko::find(10);
        $order = order::where('id_member',$id_member)->get();
        
        return view('apk.order.index',compact('title','order', 'toko'));
    }

    public function getOrderDetail($id){

        $id_member = Auth::user()->id_member;
        $order = order::whereIdMember($id_member)->findOrFail($id);
        $toko = toko::find(10);

        $title = "Tampil Order $id";

        $total = 0;

        foreach ($order->detail($id) as $value) {
            $jumlah = $value->jumlah * $value->harga;
            $total = $total + $jumlah;
        }
        
        $metode = explode('-', $order->metode);
        if(count($metode) > 1)
            $metode = method::whereIdToko($order->id_toko)->find($metode[1]);
        else
            $metode = null;

        return view('apk.order.show',compact('title','order','total','metode', 'toko'));
    }
}
