<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Mail;
use App\Mail\KirimEmail;

use App\Models\keranjang;
use App\Models\keranjangDetail;
use App\Models\order;
use App\Models\toko;
use App\Models\produk;
use App\Models\orderDetail;
use App\Models\member;
use App\Models\method;

use Str;

class KeranjangController extends Controller
{
    //
    public function index($uid){
        $toko = toko::where('seo_toko',$uid)->first();

    	$id_member = Auth::user()->id_member;
    	$keranjang = keranjang::whereIdMember($id_member)->whereIdToko($toko->id_toko)->first();
    	if($keranjang==null){
    		$isiKeranjang = null;
    	}else{
    		$isiKeranjang = DB::table('otw_keranjang_detail')->select('*' )->join('otw_produk', 'otw_produk.id_produk', '=', 'otw_keranjang_detail.id_produk')->where('otw_keranjang_detail.id_keranjang',$keranjang->id_keranjang)->orderBy('otw_keranjang_detail.updated_at','desc')->get();
    	}

    	$title = "Keranjang belanjaKu";

    	return view('frontend.keranjang.index',compact('title','keranjang','isiKeranjang','toko'));
    }

    public function edit($uid, $id){
        $toko = toko::where('seo_toko',$uid)->first();

    	$title = "Keranjang belanjaKu";

    	$id_member = Auth::user()->id_member;
    	$keranjang = keranjang::whereIdMember($id_member)->whereIdToko($toko->id_toko)->first();

    	if($keranjang==null){
    		$produk = null;
    	}else{
	    	$produk = DB::table('otw_keranjang_detail')->select('*' )->join('otw_produk', 'otw_produk.id_produk', '=', 'otw_keranjang_detail.id_produk')->where('otw_keranjang_detail.id_detail',$id)->first();
	    }

    	return view('frontend.keranjang.ubah',compact('title','produk','keranjang','toko'));
    }

    public function store($uid, Request $request){
    	$request->validate([
            'produk'=>'required',
            'jumlah'=>'required'
        ]);

        $toko = toko::where('seo_toko',$uid)->first();

        $produk = produk::find($request->get('produk'));

        if($produk->stok <= 0){
            return redirect()->route('produk.show',[$produk->id_produk,Str::slug($produk->nm_produk)])->with('error', 'Stok barang sudah habis!');
        }

    	$id_member = Auth::user()->id_member;

        $keranjang = keranjang::whereIdMember($id_member)->whereIdToko($toko->id_toko)->first();

        if($keranjang == null){ // belum ada keranjang
        	$keranjang = new keranjang;
        	$keranjang->id_member = $id_member;
            $keranjang->id_toko = $toko->id_toko;
	        $keranjang->save();
        }

        $isiKeranjang = keranjangDetail::where('id_keranjang',$keranjang->id_keranjang)->where('id_produk',$request->get('produk'))->first();
        if($isiKeranjang == null){ //belum ada dalam keranjang 
        	$isiKeranjang = new keranjangDetail;
        	$isiKeranjang->id_keranjang = $keranjang->id_keranjang;
	        $isiKeranjang->id_produk = $request->get('produk');
	        $isiKeranjang->jumlah = $request->get('jumlah');
	        $isiKeranjang->save();
        }else{ //sudah ada dalam keranjang
        	return redirect()->route('keranjang.index')->with('error', 'Sudah ada di dalam keranjang!');    	
        }

        return redirect()->route('keranjang.index')->with('success', 'Berhasil ditambahkan!');

    }

    public function ubah(Request $request, $uid, $id){

    	$request->validate([
            'jumlah'=>'required'
        ]);

        $toko = toko::where('seo_toko',$uid)->first();

        $id_member = Auth::user()->id_member;

        $keranjang = keranjang::whereIdMember($id_member)->whereIdToko($toko->id_toko)->first();

        if($keranjang==null){
        	return redirect()->route('keranjang.index')->with('error', 'Server tidak merespon!'); 
        }

    	$isiKeranjang = keranjangDetail::find($id);
    	$isiKeranjang->jumlah = $request->get('jumlah');
    	$isiKeranjang->save();

        return redirect()->route('keranjang.index')->with('success', 'Berhasil diubah!');

    }

    public function destroy($uid, $id){

    	$id_member = Auth::user()->id_member;
        $toko = toko::where('seo_toko',$uid)->first();

        $keranjang = keranjang::whereIdMember($id_member)->whereIdToko($toko->id_toko)->first();

        if($keranjang==null){
        	return redirect('keranjang')->with('error', 'Server tidak merespon!'); 
        }
        $isiKeranjang = keranjangDetail::find($id);
        $isiKeranjang->delete();

        return redirect()->route('keranjang.index')->with('success', 'Item berhasil dihapus!');
    }

    public function antar($uid){
        $toko = toko::where('seo_toko',$uid)->first();

        $title = "Antar belanjaku";

        $id_member = Auth::user()->id_member;
        $keranjang = keranjang::whereIdMember($id_member)->whereIdToko($toko->id_toko)->first();

        if($keranjang==null){
            return redirect()->route('keranjang.index')->with('error', 'Server tidak merespon!'); 
        }
        $isiKeranjang = DB::table('otw_keranjang_detail')->select('*')->join('otw_produk', 'otw_produk.id_produk', '=', 'otw_keranjang_detail.id_produk')->where('otw_keranjang_detail.id_keranjang',$keranjang->id_keranjang)->orderBy('otw_keranjang_detail.updated_at','desc')->get();

        $total = 0;
        foreach ($isiKeranjang as $value) {
            $jumlah = $value->jumlah * ($value->harga-$value->diskon);
            $total = $total + $jumlah;
        }

        // $bank = DB::table('otw_bank')->select('*')->join('otw_bank_toko', 'otw_bank.id_bank', '=', 'otw_bank_toko.id_bank')->where('otw_bank_toko.id_toko', $toko->id_toko)->get();
        $metode = method::whereIdToko($toko->id_toko)->whereTampil(1)->whereHapus(0)->get();

        return view('frontend.keranjang.antar',compact('title','keranjang','isiKeranjang','toko', 'total', 'metode'));
    }

    public function bayar($uid, Request $request){

        $toko = toko::where('seo_toko',$uid)->first();

        $request->validate([
            'nohp'=>'required',
            'nama'=>'required',
            'alamat'=>'required',
            'metode'=>'required'
        ]);

        $title = "Invoice Belanjaku";
        $id_member = Auth::user()->id_member;
        $keranjang = keranjang::whereIdMember($id_member)->whereIdToko($toko->id_toko)->first();

        if($keranjang==null){
            return redirect()->route('keranjang.index')->with('error', 'Server tidak merespon!'); 
        }

        $order = new order;
        $order->id_member = $id_member;
        $order->id_toko = $toko->id_toko;
        $order->nohp = $request->get('nohp');
        $order->nama = $request->get('nama');
        $order->alamat = $request->get('alamat');
        $order->metode = $request->get('metode');
        $order->note = $keranjang->note;
        $order->total = 0;
        $order->save();

        $isiKeranjang = DB::table('otw_keranjang_detail')->select('otw_keranjang_detail.*','otw_produk.id_produk','otw_produk.harga','otw_produk.diskon','otw_produk.modal')->join('otw_produk', 'otw_produk.id_produk', '=', 'otw_keranjang_detail.id_produk')->where('otw_keranjang_detail.id_keranjang',$keranjang->id_keranjang)->orderBy('otw_keranjang_detail.updated_at','desc')->get();

        $total = 0;

        foreach($isiKeranjang as $item){
            $orderDetail = new orderDetail;
            $orderDetail->id_order = $order->id_order;
            $orderDetail->id_produk = $item->id_produk;
            $orderDetail->harga = $item->harga-$item->diskon;
            $orderDetail->jumlah = $item->jumlah;
            $orderDetail->diskon = $item->diskon;
            $orderDetail->modal = $item->modal;
            $orderDetail->save();

            $total = $total + (($item->harga-$item->diskon) * $item->jumlah);
        }

        $order->total = $total + $toko->ongkir;
        $order->save();

        DB::table('otw_keranjang')->where('id_keranjang', '=', $keranjang->id_keranjang)->delete();
        DB::table('otw_keranjang_detail')->where('id_keranjang', '=', $keranjang->id_keranjang)->delete();

        if(env('APP_ENV') != 'local'){
            if($toko->email!=""){
                Mail::to($toko->email)->send(new KirimEmail($order));
            }
        }

        if(env('APP_ENV') == 'local')
            Mail::to('hendra.randy.nomura92@gmail.com')->send(new KirimEmail($order));  

        $member = member::find($id_member);
        $member->alamat = $request->get('alamat');
        $member->save();

        return redirect()->route('order.show', ['id' => $order->id_order]);
    }
    
    public function postNote($uid, Request $req){
        $toko = toko::where('seo_toko',$uid)->first();
        $id_member = Auth::user()->id_member;
        $keranjang = keranjang::whereIdMember($id_member)->whereIdToko($toko->id_toko)->first();

        if($keranjang==null){
            return redirect()->route('keranjang.index')->with('error', 'Server tidak merespon!'); 
        }
        $keranjang = keranjang::find($keranjang->id_keranjang);
        $keranjang->note = $req->note;
        $keranjang->save();
        
        return redirect()->route('keranjang.index')->with('success', 'Note berhasil ditambahkan');
    }
}
