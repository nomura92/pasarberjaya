<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\toko;
use App\Models\order;
use App\Models\orderDetail;
use App\Models\method;

class OrderController extends Controller
{
    //

    public function index($uid){
        $toko = toko::where('seo_toko',$uid)->first();

    	$title = "Tampil Semua Order";
    	$id_member = Auth::user()->id_member;

    	$order = order::where('id_member',$id_member)->get();
    	
    	return view('frontend.order.index',compact('title','order','toko'));
    }

    public function show($uid, $id){
        $id_member = Auth::user()->id_member;
        $order = order::whereIdMember($id_member)->findOrFail($id);

        $toko = toko::where('seo_toko',$uid)->first();

    	$title = "Tampil Order $id";

        $total = 0;

        foreach ($order->detail($id) as $value) {
            $jumlah = $value->jumlah * $value->harga;
            $total = $total + $jumlah;
        }
        
        $metode = explode('-', $order->metode);
        if(count($metode) > 1)
            $metode = method::whereIdToko($order->id_toko)->find($metode[1]);
        else
            $metode = null;

    	return view('frontend.order.show',compact('title','order','toko','total','metode'));
    }
}
