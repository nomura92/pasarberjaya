<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\toko;
use App\Models\member;

use Hash;

class MemberController extends Controller
{
    //

    public function index($uid){
    	$toko = toko::where('seo_toko',$uid)->first();
    	$title = "Halaman member";

    	return view('frontend.member.index',compact('title','toko'));
    }

    public function toko($uid){
        $toko = toko::where('seo_toko',$uid)->first();
        $title = "Halaman profil toko";

        return view('frontend.member.toko',compact('title','toko'));
    }

    public function profil($uid){
    	$toko = toko::where('seo_toko',$uid)->first();
    	$title = "Halaman profil member";

    	return view('frontend.member.profil',compact('title','toko'));
    }

    public function profilUpdate(Request $request){
        $request->validate([
            'nama'=>'required|string'
        ]);

        $member = member::find(Auth::user()->id_member);
        $member->nama = $request->get('nama');
        $member->alamat = $request->get('alamat');
        $member->save();

        return redirect()->route('member.profil')->with('success','Berhasil update profil');
    }

    public function password(Request $request){
        $request->validate([
            'password'=>'required|string|confirmed',
        ]);

        $member = member::find(Auth::user()->id_member);
        $member->password = Hash::make($request->get('nama'));
        $member->save();

        return redirect()->route('member.profil')->with('success','Berhasil update password');
    }
}
