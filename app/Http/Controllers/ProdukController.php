<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\produk;
use App\Models\produkGambar;
use App\Models\toko;

class ProdukController extends Controller
{
    //
    public function show( $uid , $id, $slug){
    	$toko = toko::where('seo_toko',$uid)->first();

        $produk = produk::where('id_toko', $toko->id_toko)->where('id_produk', $id)->first();
        $title = $produk->nm_produk;

        $gambar = produkGambar::where('id_produk', $id)->get();

        return view('frontend.produk.detail', compact('title','produk','toko','gambar'));
    }

    public function store(Request $request){
    	
    }
}
