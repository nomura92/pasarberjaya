<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\URL;
use View;

use App\Models\toko;
use App\Models\newsToko;
use Auth;
use DB;
use App\Models\notifikasi;

class checkToko
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $toko = toko::where('seo_toko',$request->uid)->first();
        $allToko = toko::where('hapus', 0)->where('blokir', 0)->get();

        if($toko==null){
            return abort(404,'Toko tidak ditemukan.');
        }

        if($toko->status == 0){
            return abort(404,'Toko tidak aktif.');
        }

        if(!Auth::user()){
            $isiKeranjang = 0;
            $news = collect();
        }else{
          $isiKeranjang = 0;
          if($request->uid){
              $toko = toko::where('seo_toko', $request->uid)->first();
              $id_member = Auth::user()->id_member;
              $isiKeranjang = DB::table('otw_keranjang_detail')->selectRaw('(otw_produk.harga-otw_produk.diskon)*otw_keranjang_detail.jumlah as total' )->join('otw_produk', 'otw_produk.id_produk', '=', 'otw_keranjang_detail.id_produk')->join('otw_keranjang','otw_keranjang.id_keranjang','=','otw_keranjang_detail.id_keranjang')->where('otw_keranjang.id_member', $id_member)->where('otw_keranjang.id_toko', $toko->id_toko)->get();

              $total = 0;
              foreach($isiKeranjang as $item){
                  $total = $total + $item->total;
              }
              $isiKeranjang = $total;

                $news = notifikasi::where('id_toko', $toko->id_toko)->where('id_member', $id_member)->get();
            }else{
              $news = collect();
            }
        }

        View::share('duitKeranjang', "Rp ".number_format($isiKeranjang,0,',','.'));
        View::share('jmlNews', $news);

        URL::defaults(['uid' => $request->uid]);

        $news = newsToko::where('id_toko',$toko->id_toko)->where('tampil',1)->where('hapus',0)->get();
        View::share('news', $news);
        View::share('allToko', $allToko);

        return $next($request);
    }
}
