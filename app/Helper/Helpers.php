<?php

namespace App\Helper;

use Image;

class Helpers{

	public static function UploadImg($uploadedFile, $thumbnailPath = null){

        $thumbnailImage = Image::make($uploadedFile);
        if(!$thumbnailPath)
        	$thumbnailPath = 'uploads/';
        if (!file_exists($thumbnailPath)) {
            mkdir($thumbnailPath, 0755, true);
        }
        $thumbnailImage->resize(600, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        $filename = str_replace(' ','',time().$uploadedFile->getClientOriginalName());
        $thumbnailImage->save($thumbnailPath.$filename);

        return $thumbnailPath.$filename;
	}
}